﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BreathingGames.ArcadeVertShooter
{
    public class Player : MonoBehaviour
    {
        public GameObject targetRestartPos;
        public GameObject destructionFX;
        public static Player instance;
        public int life;
        public Text liveTxt;

        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        private void Start()
        {
            liveTxt.text = life.ToString();
        }

        // method for damage processing by 'Player'
        public void GetDamage(int damage)
        {
            StartCoroutine(Destruction());           
        }

        // generating destruction visual effect and destroying the Player object
        IEnumerator Destruction()
        {
            Instantiate(destructionFX, transform.position, Quaternion.identity);
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            transform.GetChild(3).gameObject.SetActive(false);
            life--;
            liveTxt.text = life.ToString();

            if (life == 0)
            {
                SceneManager.LoadScene("Shooter 04  Last");
            }
            else
            {
                yield return new WaitForSeconds(1.5f);
                GetComponent<SpriteRenderer>().enabled = true;
                GetComponent<Collider2D>().enabled = true;
                transform.GetChild(3).gameObject.SetActive(true);
                instance.transform.position = targetRestartPos.transform.position;
            }
        }
    }
}