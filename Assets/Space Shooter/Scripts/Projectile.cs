﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class Projectile : MonoBehaviour
    {

        [Tooltip("Damage which a projectile deals to another object. Integer")]
        public int damage;

        [Tooltip("Whether the projectile belongs to the ‘Enemy’ or to the ‘Player’")]
        public bool enemyBullet;

        [Tooltip("Whether the projectile is destroyed in the collision, or not")]
        public bool destroyedByCollision;

        /// <summary>
        /// Defines the damage and defines whether the projectile belongs to the ‘Enemy’ or to the ‘Player’, 
        /// whether the projectile is destroyed in the collision, or not and amount of damage.
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //if object is 'player' or 'enemy send command to receive damage
            if (enemyBullet && collision.tag == "Player")
            {
                Player.instance.GetDamage(damage);
                if (destroyedByCollision)
                    Destruction();
            }
            else if (!enemyBullet && collision.tag == "Enemy")
            {
                collision.GetComponent<Enemy>().GetDamage(damage);
                if (destroyedByCollision)
                    Destruction();
            }
            else if (!enemyBullet && collision.tag == "Boss")
            {
                collision.GetComponent<Boss>().GetDamage(damage);
                if (destroyedByCollision)
                    Destruction();
            }
        }

        void Destruction()
        {
            Destroy(gameObject);
        }
    }
}