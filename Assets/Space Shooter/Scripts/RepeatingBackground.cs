﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class RepeatingBackground : MonoBehaviour
    {
        // attaches to ‘Background’ object, and would move it up if the object went down below the viewport border. 
        [Tooltip("vertical size of the sprite in the world space. Attach box collider2D to get the exact size")]
        public float verticalSize;

        void Update()
        {
            //if sprite goes down below the viewport move the object up above the viewport
            if (transform.position.y < -verticalSize)
            {
                RepositionBackground();
            }
        }

        /// <summary>
        /// creating the effect of infinite movement. 
        /// </summary>
        void RepositionBackground()
        {
            Vector2 groundOffSet = new Vector2(0, verticalSize * 2f);
            transform.position = (Vector2)transform.position + groundOffSet;
        }
    }
}