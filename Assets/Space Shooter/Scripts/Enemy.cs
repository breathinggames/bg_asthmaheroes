﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class Enemy : MonoBehaviour
    {

        #region FIELDS
        [Tooltip("Health points in integer")]
        public int health;

        [Tooltip("Enemy's projectile prefab")]
        public GameObject Projectile;

        [Tooltip("VFX prefab generating after destruction")]
        public GameObject destructionVFX;
        public GameObject hitEffect;

        // probability of 'Enemy's' shooting during tha path
        [HideInInspector] public int shotChance;

        // max and min time for shooting from the beginning of the path
        [HideInInspector] public float shotTimeMin, shotTimeMax;

        private AudioSource audioData;
        #endregion

        void Start()
        {
            audioData = GetComponent<AudioSource>();
            Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
            Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
            Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
        }

        void ActivateShooting()
        {
            // if random value less than shot probability, make a shot
            if (Random.value < (float)shotChance / 100)                             
            {
                audioData.Play(0);
                Instantiate(Projectile, gameObject.transform.position, Quaternion.identity);                
            }
        }

        /// <summary>
        /// method of getting damage for the Enemy
        /// reducing health for damage value, if health is less than 0, starting destruction procedure
        /// </summary>
        /// <param name="damage"></param>
        public void GetDamage(int damage)
        {
            health -= damage;           
            if (health <= 0)
                Destruction();
            else
                Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
        }

        /// <summary>
        /// if Enemy collides with Player, Player gets the damage equal to projectile's damage value
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                if (Projectile.GetComponent<Projectile>() != null)
                    Player.instance.GetDamage(Projectile.GetComponent<Projectile>().damage);
                else
                    Player.instance.GetDamage(1);
            }
        }

        /// <summary>
        /// method of destroying the Enemy
        /// </summary>
        void Destruction()
        {
            Instantiate(destructionVFX, transform.position, Quaternion.identity);
            Destroy(gameObject);

            if (this.name == "Boss")
            {
                Instantiate(destructionVFX, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}