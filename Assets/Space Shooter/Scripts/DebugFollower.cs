﻿using UnityEngine;

public class DebugFollower : MonoBehaviour
{
    private bool cinManager = false;

    void Awake()
    {
        if (!cinManager)
        {
            DontDestroyOnLoad(this.gameObject);
            cinManager = true;
        }
    }
}
