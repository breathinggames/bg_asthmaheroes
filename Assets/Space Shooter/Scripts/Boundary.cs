﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class Boundary : MonoBehaviour
    {
        // defines the size of the ‘Boundary’ depending on Viewport. When objects go beyond the ‘Boundary’
        BoxCollider2D boundareCollider;

        //receiving collider's component and changing boundary borders
        private void Start()
        {
            boundareCollider = GetComponent<BoxCollider2D>();
            ResizeCollider();
        }

        //changing the collider's size up to Viewport's size multiply 1.5
        void ResizeCollider()
        {
            Vector2 viewportSize = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)) * 2;
            viewportSize.x *= 0.9f;
            viewportSize.y *= 1.5f;
            boundareCollider.size = viewportSize;
        }

        //when another object leaves collider they are destroyed or deactivated
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Projectile")
            {
                Destroy(collision.gameObject);
            }
            else if (collision.tag == "Bonus")
                Destroy(collision.gameObject);
        }
    }
}