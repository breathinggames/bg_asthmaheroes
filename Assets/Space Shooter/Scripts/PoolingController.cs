﻿using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    [System.Serializable]
    public class PoolingObjects
    {
        public GameObject pooledPrefab;
        public int count;
    }
}

namespace BreathingGames.ArcadeVertShooter
{
    public class PoolingController : MonoBehaviour
    {

        [Tooltip("Your 'pooling' objects. Add new element and add the prefab to create the object prefab")]
        public PoolingObjects[] poolingObjectsClass;

        //The list where 'pooling' objects will be stored
        List<GameObject> pooledObjectsList = new List<GameObject>();

        //unique class instance for the easy access
        public static PoolingController instance;

        void Awake()
        {
            if (instance == null)
                instance = this;
        }

        void Start()
        {
            CreateNewList();
        }

        /// <summary>
        /// Create the new list of 'pooling' objects
        /// </summary>
        void CreateNewList()
        {
            //for each prefab create the needed amount of objects and deactivate them
            for (int i = 0; i < poolingObjectsClass.Length; i++)
            {
                for (int k = 0; k < poolingObjectsClass[i].count; k++)
                {
                    GameObject newObj = Instantiate(poolingObjectsClass[i].pooledPrefab, transform);
                    pooledObjectsList.Add(newObj);
                    newObj.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Looking for the needed object by prefab name and return it
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public GameObject GetPoolingObject(GameObject prefab)
        {
            string cloneName = GetCloneName(prefab);
            for (int i = 0; i < pooledObjectsList.Count; i++)
            {
                if (!pooledObjectsList[i].activeSelf && pooledObjectsList[i].name == cloneName)
                {
                    return pooledObjectsList[i];
                }
            }

            // if there is no object needed create the new one
            return AddNewObject(prefab);
        }

        /// <summary>
        /// create the new object and add it to the list
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        GameObject AddNewObject(GameObject prefab)
        {
            GameObject newObj = Instantiate(prefab, transform);
            pooledObjectsList.Add(newObj);
            newObj.SetActive(false);
            return newObj;
        }

        string GetCloneName(GameObject prefab)
        {
            return prefab.name + "(Clone)";
        }
    }
}