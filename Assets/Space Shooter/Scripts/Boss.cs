﻿using BreathingGames.ArcadeVertShooter;
using UnityEngine;

public class Boss : MonoBehaviour {

    [Tooltip("Health points in integer")]
    public int health;

    [Tooltip("VFX prefab generating after destruction")]
    public GameObject destructionVFX;
    public GameObject hitEffect;

    [SerializeField] private int numberOfProjectiles;
    [SerializeField] private GameObject projectile;

    private Vector2 startPoint;
    private float radius;
    private float moveSpeed;

    private AudioSource audioData;
    private float cooldown = 0.0f;

    public GameObject endGame;

    void Start () {
        radius = 5f;
        moveSpeed = 5f;

        audioData = GetComponent<AudioSource>();
    }
	
	void Update ()
    {
        CalculateCooldown();
    }

    private void CalculateCooldown()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }
        else
        {
            startPoint = this.transform.position;
            audioData.Play(0);
            SpawnProjectiles(numberOfProjectiles);
            cooldown = 1f;
        }
    }

    private void SpawnProjectiles(int numberOfProjectiles)
    {
        float angleStep = 360f / numberOfProjectiles;
        float angle = 0f;

        for (int i = 0; i < numberOfProjectiles - 1; i++)
        {
            float projectileXpos = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
            float projectileYpos = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

            Vector2 projectileVector = new Vector2(projectileXpos, projectileYpos);
            Vector2 projectileMoveDir = (projectileVector - startPoint).normalized * moveSpeed;

            GameObject proj = Instantiate(projectile, startPoint, Quaternion.identity);
            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveDir.x, projectileMoveDir.y);

            angle += angleStep;
        }
    }

    /// <summary>
    /// method of getting damage for the Enemy
    /// reducing health for damage value, if health is less than 0, starting destruction procedure
    /// </summary>
    /// <param name="damage"></param>
    public void GetDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            Destruction();
        else
            Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
    }

    /// <summary>
    /// if Enemy collides with Player, Player gets the damage equal to projectile's damage value
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (projectile.GetComponent<Projectile>() != null)
                Player.instance.GetDamage(projectile.GetComponent<Projectile>().damage);
            else
                Player.instance.GetDamage(1);
        }
    }

    /// <summary>
    /// method of destroying the Enemy
    /// </summary>
    void Destruction()
    {
        Instantiate(destructionVFX, transform.position, Quaternion.identity);
        endGame.SetActive(true);
        Destroy(gameObject);        
    }
}
