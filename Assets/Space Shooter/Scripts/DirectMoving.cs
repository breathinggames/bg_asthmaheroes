﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class DirectMoving : MonoBehaviour
    {
        [Tooltip("Moving speed on Y axis in local space")]
        public float speed;

        // moving the object along the Y-axis with the defined speed
        void Update()
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }

        public void SpeedUp()
        {
            speed = -0.3f;
        }

        public void SpeedDown()
        {
            speed = 0f;
        }
    }
}