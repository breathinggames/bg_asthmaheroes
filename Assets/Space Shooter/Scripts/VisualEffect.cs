﻿using System.Collections;
using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class VisualEffect : MonoBehaviour
    {
        // This script attaches to ‘VisualEffect’ objects. 

        [Tooltip("the time after object will be destroyed")]
        public float destructionTime;

        private void OnEnable()
        {
            //launching the timer of destruction
            StartCoroutine(Destruction());
        }

        /// <summary>
        /// It destroys or deactivates them after the defined time.
        /// </summary>
        /// <returns></returns>
        IEnumerator Destruction() //wait for the estimated time, and destroying or deactivating the object
        {
            yield return new WaitForSeconds(destructionTime);
            Destroy(gameObject);
        }
    }
}