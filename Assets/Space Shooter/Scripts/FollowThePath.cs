﻿using UnityEngine;
using System;

namespace BreathingGames.ArcadeVertShooter
{
    public class FollowThePath : MonoBehaviour
    {
        // path points which passes the Enemy 
        [HideInInspector] public Transform[] path; 
        [HideInInspector] public float speed;

        // whether Enemy rotates in path direction or not
        [HideInInspector] public bool rotationByPath;

        // if true, Enemy returns to the path starting point after completing the path
        [HideInInspector] public bool loop;         
        float currentPathPercent;  

        // path points in vector3
        Vector3[] pathPositions;                
        [HideInInspector] public bool movingIsActive;

        /// <summary>
        /// setting path parameters for the Enemy and sending the Enemy to the path starting point
        /// </summary>
        public void SetPath()
        {
            currentPathPercent = 0;

            // transform path points to vector3
            pathPositions = new Vector3[path.Length];       
            for (int i = 0; i < pathPositions.Length; i++)
            {
                pathPositions[i] = path[i].position;
            }

            //sending the enemy to the path starting point
            transform.position = NewPositionByPath(pathPositions, 0); 
            if (!rotationByPath)
                transform.rotation = Quaternion.identity;
            movingIsActive = true;
        }

        void Update()
        {
            if (movingIsActive)
            {
                // every update calculating current path percentage according to the defined speed
                currentPathPercent += speed / 100 * Time.deltaTime;

                // moving the Enemy to the path position, calculated in method NewPositionByPath
                transform.position = NewPositionByPath(pathPositions, currentPathPercent);

                // rotating the Enemy in path direction, if set rotationByPath
                if (rotationByPath)                           
                {
                    transform.right = Interpolate(CreatePoints(pathPositions), currentPathPercent + 0.01f) - transform.position;
                    transform.Rotate(Vector3.forward * 90);
                }

                // when the path is complete
                if (currentPathPercent > 1)                    
                {
                    // when loop is set, moving to the path starting point; if not, destroying or deactivating the Enemy
                    if (loop)                                   
                        currentPathPercent = 0;
                    else
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }

        Vector3 NewPositionByPath(Vector3[] pathPos, float percent)
        {
            return Interpolate(CreatePoints(pathPos), currentPathPercent);
        }

        Vector3 Interpolate(Vector3[] path, float t)
        {
            int numSections = path.Length - 3;
            int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
            float u = t * numSections - currPt;
            Vector3 a = path[currPt];
            Vector3 b = path[currPt + 1];
            Vector3 c = path[currPt + 2];
            Vector3 d = path[currPt + 3];
            return 0.5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) + (-a + c) * u + 2f * b);
        }

        /// <summary>
        /// using interpolation method calculating the path along the path points
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Vector3[] CreatePoints(Vector3[] path)
        {
            Vector3[] pathPositions;
            Vector3[] newPathPos;
            int dist = 2;
            pathPositions = path;
            newPathPos = new Vector3[pathPositions.Length + dist];
            Array.Copy(pathPositions, 0, newPathPos, 1, pathPositions.Length);
            newPathPos[0] = newPathPos[1] + (newPathPos[1] - newPathPos[2]);
            newPathPos[newPathPos.Length - 1] = newPathPos[newPathPos.Length - 2] + (newPathPos[newPathPos.Length - 2] - newPathPos[newPathPos.Length - 3]);
            if (newPathPos[1] == newPathPos[newPathPos.Length - 2])
            {
                Vector3[] LoopSpline = new Vector3[newPathPos.Length];
                Array.Copy(newPathPos, LoopSpline, newPathPos.Length);
                LoopSpline[0] = LoopSpline[LoopSpline.Length - 3];
                LoopSpline[LoopSpline.Length - 1] = LoopSpline[2];
                newPathPos = new Vector3[LoopSpline.Length];
                Array.Copy(LoopSpline, newPathPos, LoopSpline.Length);
            }
            return (newPathPos);
        }
    }
}