﻿using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.Character
{
    public class PhysicsObject : MonoBehaviour
    {
        public float minGroundNormalY = 0.65f;
        public float gravityModifier = 1f;

        public bool isOnLadder = false;

        protected bool isGrounded;
        protected Vector2 targetVelocity;
        protected Vector2 groundNormal;
        protected Vector2 velocity;
        protected Rigidbody2D rb2d;
        protected ContactFilter2D contactFilter;
        protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
        protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

        // make sure the distance from the other collider is active if over a min distance
        protected const float MINMOVEDISTANCE = 0.001f;
        // make sure our collider never get stuck inside another collider
        protected const float SHELLRADIUS = 0.01f;

        void OnEnable()
        {
            rb2d = GetComponent<Rigidbody2D>();
        }

        void Start()
        {
            // ignore collider if it is a trigger
            contactFilter.useTriggers = false;

            // use the settings from the collision matrix in physics2dSettings from the edit menu
            contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            contactFilter.useLayerMask = true;
        }

        // Update is called once per frame
        void Update()
        {
            targetVelocity = Vector2.zero;
            ComputeVelocity();
        }

        protected virtual void ComputeVelocity()
        {

        }

        void FixedUpdate()
        {
            if (!isOnLadder)
            {
                // gradually move this object towards the ground using gravity force
                velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
                velocity.x = targetVelocity.x;

                // isGrounded is alwya false before a collider is registered
                isGrounded = false;

                // the var used for movement displacement value
                Vector2 deltaPosition = velocity * Time.deltaTime;

                // this will store the direction we are trying to move on the ground
                // wqe are calculing it twice, once for the x and once for the y and so, we are calling Movement twice, once for each dimension
                Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);
                Vector2 move = moveAlongGround * deltaPosition.x;
                Movement(move, false);

                // use the move vector to apply movement to our object
                move = Vector2.up * deltaPosition.y;
                Movement(move, true);
            }
        }

        // move the object from its rigidbody2d
        void Movement(Vector2 move, bool yMovement)
        {
            // check if distance to the ground is > than a minimum value to prevent object from constantly checking if it is on the ground
            float distance = move.magnitude;
            if (distance > MINMOVEDISTANCE)
            {
                // checked if our object collider is going to overlap another object collider
                int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + SHELLRADIUS);

                // list of current active contacts
                hitBufferList.Clear();
                for (int i = 0; i < count; i++)
                {
                    hitBufferList.Add(hitBuffer[i]);
                }

                // compare the hit buffer to a list of normals we are colliding it
                for (int i = 0; i < hitBufferList.Count; i++)
                {
                    // this will be used to determine if the player is grounded and which animation will be played depending on the result
                    Vector2 currentNormal = hitBufferList[i].normal;
                    if (currentNormal.y > minGroundNormalY)
                    {
                        // the normal allows us to make sure that it is the ground we are checking and not the wall to determine if he is grounded
                        isGrounded = true;
                        if (yMovement)
                        {
                            groundNormal = currentNormal;
                            currentNormal.x = 0;
                        }
                    }

                    // we get the difference between the velocity and the current normal to see if we must subtract to their current velocity
                    float projection = Vector2.Dot(velocity, currentNormal);
                    if (projection < 0)
                    {
                        // we cancel out the part of the velocity that would be stopped by a collision
                        velocity = velocity - projection * currentNormal;
                    }

                    // prevent us from getting stuck inside other colliders
                    float modifiedDistance = hitBufferList[i].distance - SHELLRADIUS;
                    distance = modifiedDistance < distance ? modifiedDistance : distance;
                }
            }

            rb2d.position = rb2d.position + move.normalized * distance;
        }
    }
}