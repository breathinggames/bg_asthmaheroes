﻿using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraFollowDebug : MonoBehaviour
{
    private CinemachineVirtualCamera cine;
    private Scene m_Scene;
    private string sceneName;


    void Awake()
    {
        cine = FindObjectOfType<CinemachineVirtualCamera>();
        Init();
    }

    private void Update()
    {
        if (cine.Follow == null)
        {
            CallPlayer();
        }
    }

    private void Init()
    {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;

        switch (sceneName)
        {
            case "06a HQ":
            case "06b HQ Inside":
            case "07a Main Hub":
            case "09 Scene 01":
            case "14 Interstice":
                cine.m_Lens.OrthographicSize = 4f;
                break;
            case "07b House":
            case "07c House":
            case "08 World Map":
            case "07c-2 House Intro":
            case "07c-4 House OrderGame":
            case "07c-6 House OrderEnd":
                cine.m_Lens.OrthographicSize = 5f;
                break;
            default:
                break;
        }
    }

    private void CallPlayer()
    {
        cine.Follow = GameObject.FindGameObjectWithTag("Player").transform;
    }
}
