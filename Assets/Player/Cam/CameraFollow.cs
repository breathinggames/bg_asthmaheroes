﻿using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class CameraFollow : MonoBehaviour {

    private CinemachineVirtualCamera cine;
    private bool cinManager = false;
    private Scene m_Scene;
    private string sceneName;

    void Awake()
    {
        DestroyVcam();

        if (!cinManager)
        {
            DontDestroyOnLoad(this.gameObject);
            cinManager = true;
        }

        cine = FindObjectOfType<CinemachineVirtualCamera>();
    }

    void Start()
    {
        DestroyVcam();
        cine = FindObjectOfType<CinemachineVirtualCamera>();
    }

    void Update()
    {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;

        DestroyVcam();

        if (transform.parent != null && cine == null && transform.parent.name == "Debug")
        {
            cine = FindObjectOfType<CinemachineVirtualCamera>();
            CallPlayer();
        }
        else if (GameObject.FindGameObjectWithTag("Player") && cine == null && sceneName != "TransitionToShooter")
        {
            cine = FindObjectOfType<CinemachineVirtualCamera>();
            CallPlayer();
        }
        else if (cine == null && sceneName == "07c-3 House AssGame")
        {
            cine = FindObjectOfType<CinemachineVirtualCamera>();
        }

        switch (sceneName)
        {
            case "06a HQ":
            case "06b HQ Inside":
            case "07a Main Hub":
            case "09 Scene 01":
            case "14 Interstice":
                cine.m_Lens.OrthographicSize = 4f;
                break;
            case "07b House":
            case "07c House":
            case "08 World Map":           
                cine.m_Lens.OrthographicSize = 5f;
                break;
            case "07c-3 House AssGame":
                cine.m_Lens.OrthographicSize = 6f;
                break;
            default:
                break;
        }
    }

    public void CallPlayer()
    {
        cine.Follow = GameObject.FindGameObjectWithTag("Player").transform;
        cine.m_Lens.OrthographicSize = 4f;
    }

    private void DestroyVcam()
    {
        if (sceneName == "TransitionToShooter")
        {
            Destroy(this.gameObject);
        }
    }
}
