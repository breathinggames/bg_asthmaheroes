﻿using BreathingGames.UISystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BreathingGames.Character
{
    public class PlayerPlatformerController : PhysicsObject
    {
        public float maxSpeed = 7;
        public float climbSpeed = 2;
        public float jumpTakeOffSpeed = 7;
        public float runSpeed = 3f;
        public bool isRunning = false;

        private SpriteRenderer spriteRenderer;
        private Animator animator;
        private PlayerUI uiValueLungs;

        Scene m_Scene;
        string sceneName;

        void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
            uiValueLungs = GameObject.Find("Canvas").GetComponent<PlayerUI>();
        }

        void Start()
        {
            m_Scene = SceneManager.GetActiveScene();
            sceneName = m_Scene.name;
        }

        public void ResetValue()
        {
            maxSpeed = 0;
            climbSpeed = 0;
            jumpTakeOffSpeed = 0;
            runSpeed = 0f;
        }

        public void ReInitValue()
        {
            maxSpeed = 3;
            climbSpeed = 2;
            jumpTakeOffSpeed = 6;
            runSpeed = 3f;
        }

        // check input and update velocity
        protected override void ComputeVelocity()
        {
            Vector2 move = Vector2.zero;

            move.x = ControlFreak2.CF2Input.GetAxis("Horizontal");
            move.y = ControlFreak2.CF2Input.GetAxis("Vertical");

            targetVelocity = move * maxSpeed;

            if (ControlFreak2.CF2Input.GetButtonDown("Jump") && isGrounded && !isOnLadder)
            {
                velocity.y = jumpTakeOffSpeed;
            }
            else if (ControlFreak2.CF2Input.GetButtonUp("Jump") && !isOnLadder)   // mean that the button has been released
            {
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * 0.5f;
                }
            }
            else if (!ControlFreak2.CF2Input.GetButtonDown("Jump") && isGrounded && !isOnLadder)
            {
                velocity.y = 0;
            }
            else if (isOnLadder)
            {
                velocity.y = targetVelocity.y * climbSpeed;
            }

            if (sceneName != "07b House" && sceneName != "07c House" && sceneName != "04 My House" && sceneName != "08 World Map" && sceneName != "Epîlogue")
            {                
                if (uiValueLungs.lungCapacity.CurrentVal > 50.0f && (ControlFreak2.CF2Input.GetButton("Sprint") && (ControlFreak2.CF2Input.GetAxis("Horizontal") > 0.0 || ControlFreak2.CF2Input.GetAxis("Horizontal") < 0.0)))
                {
                    isRunning = true;
                    animator.speed = 3;
                    move.x = move.x * runSpeed;
                }
                else
                {
                    isRunning = false;
                    animator.speed = 1;
                }
            }

            // a part of this code should be in the ladder and it has to activate and deactivate isOnLadder
            if ((ControlFreak2.CF2Input.GetKey(KeyCode.UpArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.W)) && isOnLadder)
            {
                rb2d.velocity = new Vector2(0, climbSpeed);
            }
            else if ((ControlFreak2.CF2Input.GetKey(KeyCode.DownArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.S)) && isOnLadder)
            {
                rb2d.velocity = new Vector2(0, -climbSpeed);
            }
            else if ((ControlFreak2.CF2Input.GetKey(KeyCode.RightArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.D)) && isOnLadder)
            {
                rb2d.velocity = new Vector2(climbSpeed, 0);
            }
            else if ((ControlFreak2.CF2Input.GetKey(KeyCode.LeftArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.A)) && isOnLadder)
            {
                rb2d.velocity = new Vector2(-climbSpeed, 0);
            }
            else
            {
                rb2d.velocity = new Vector2(0, 0);
            }

            // flip player x facing direction depending on the direction of movement
            bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
            if (flipSprite)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
            }

            animator.SetBool("grounded", isGrounded);
            animator.SetBool("isOnLadder", isOnLadder);
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
            animator.SetFloat("velocityY", Mathf.Abs(velocity.y) / climbSpeed);

            targetVelocity = move * maxSpeed;
        }
    }
}