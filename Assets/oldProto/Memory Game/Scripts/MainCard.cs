﻿using UnityEngine;

namespace BreathingGames.MemoryGame
{
    public class MainCard : MonoBehaviour
    {
        [SerializeField] private SceneController controller;
        [SerializeField] private GameObject Card_Back;

        private int _id;
        public int id
        {
            get { return _id; }
        }

        public void OnMouseDown()
        {
            if (Card_Back.activeSelf && controller.canReveal)
            {
                Card_Back.SetActive(false);
                controller.CardRevealed(this);
            }
        }

        /// <summary>
        /// This gets the sprite renderer component and changes the property of it's sprite!
        /// </summary>
        /// <param name="id"></param>
        /// <param name="image"></param>
        public void ChangeSprite(int id, Sprite image)
        {
            _id = id;
            GetComponent<SpriteRenderer>().sprite = image; 
        }

        /// <summary>
        /// Hide card if they do not match
        /// </summary>
        public void Unreveal()
        {
            Card_Back.SetActive(true);
        }
    }
}