﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BreathingGames.MemoryGame
{
    public class SceneController : MonoBehaviour
    {
        public const int gridRows = 3;
        public const int gridCols = 5;
        public const float offsetX = 2f;
        public const float offsetY = 2f;

        [SerializeField] private MainCard originalCard;
        [SerializeField] private Sprite[] images;

        private AudioSource audioData;

        void Start()
        {
            audioData = GetComponent<AudioSource>();

            //The position of the first card. All other cards are offset from here.
            Vector3 startPos = originalCard.transform.position;

            // list all cards. have two of each for the matching game
            int[] numbers = { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4};
            numbers = ShuffleArray(numbers);

            // set the deck on start to begin the game
            StartDeckInit(startPos, numbers);
        }

        /// <summary>
        /// position the cards on the main board using a start position from where all other cards will be offset
        /// with the numbers of cards that are required for the game
        /// </summary>
        /// <param name="startPos"></param>
        /// <param name="numbers"></param>
        private void StartDeckInit(Vector3 startPos, int[] numbers)
        {
            for (int i = 0; i < gridCols; i++)
            {
                for (int j = 0; j < gridRows; j++)
                {
                    MainCard card;
                    if (i == 0 && j == 0)
                    {
                        card = originalCard;
                    }
                    else
                    {
                        card = Instantiate(originalCard) as MainCard;
                        card.transform.SetParent(GameObject.Find("Memory Game").transform, false);
                    }

                    int index = j * gridCols + i;
                    int id = numbers[index];
                    card.ChangeSprite(id, images[id]);

                    float posX = (offsetX * i) + startPos.x;
                    float posY = (offsetY * j) + startPos.y;
                    card.transform.position = new Vector3(posX, posY, startPos.z);
                }
            }
        }

        /// <summary>
        /// Randomnise the cards for a new game before placing them on the board
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        private int[] ShuffleArray(int[] numbers)
        {
            int[] newArray = numbers.Clone() as int[];
            for (int i = 0; i < newArray.Length; i++)
            {
                int tmp = newArray[i];
                int r = Random.Range(i, newArray.Length);
                newArray[i] = newArray[r];
                newArray[r] = tmp;
            }
            return newArray;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------

        private MainCard _firstRevealed;
        private MainCard _secondRevealed;
        private MainCard _lastRevealed;

        private int _score = 0;

        [SerializeField] private TextMesh scoreLabel;

        /// <summary>
        /// if the card can be turned
        /// </summary>
        public bool canReveal
        {
            get { return _lastRevealed == null; }
        }

        /// <summary>
        /// Turn the cards
        /// </summary>
        /// <param name="card"></param>
        public void CardRevealed(MainCard card)
        {
            audioData.Play(0);

            if (_firstRevealed == null)
            {
                _firstRevealed = card;
            }
            else if(_secondRevealed == null)
            {
                _secondRevealed = card;               
            }
            else
            {
                _lastRevealed = card;
                StartCoroutine(CheckMatch());
            }
        }

        /// <summary>
        /// Verify if the first card match with the second one
        /// </summary>
        /// <returns></returns>
        private IEnumerator CheckMatch()
        {
            if (_firstRevealed.id == _secondRevealed.id && _firstRevealed.id == _lastRevealed.id && _secondRevealed.id == _lastRevealed.id)
            {
                _score++;
                if (_score == 5)
                {
                    GameObject.Find("Scene").transform.Find("ColliderGround").gameObject.SetActive(true);

                    if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
                    {
                        GameObject.Find("Scene").transform.Find("Player_girl(Clone)").gameObject.SetActive(true);
                    }

                    GameObject.Find("Scene").transform.Find("Flowchart_EndGame").gameObject.SetActive(true);
                    GameObject.Find("Memory Game").SetActive(false);
                }

            }
            else
            {
                yield return new WaitForSeconds(0.5f);

                _firstRevealed.Unreveal();
                _secondRevealed.Unreveal();
                _lastRevealed.Unreveal();
            }
            _firstRevealed = null;
            _secondRevealed = null;
            _lastRevealed = null;
        }

        public void Restart()
        {
            SceneManager.LoadScene("MemoryGame");
        }
    }
}