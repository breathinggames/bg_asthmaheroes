﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PieMenu : MonoBehaviour
{
    private bool open = false;
	
	public List<string> commands;
	public List<Texture> icons;
	
	public float iconSize = 64f;
	public float spacing = 12f;
	public float speed = 8f;
	public GUISkin skin;
	
	[HideInInspector]
	public float scale;
	[HideInInspector]
	public float angle;
	
	PieMenuManager manager;

    public GameObject inventory;
    bool menuOpen;

    void Awake () {
		manager = PieMenuManager.Instance;
        inventory = GameObject.Find("Canvas").transform.Find("Inventory").gameObject;
    }

    private void Update()
    {
        if (!inventory.activeInHierarchy)
        {
            if (ControlFreak2.CF2Input.GetButtonDown("Inventory") && !open && !menuOpen)
            {
                open = true;
                manager.Show(this);
            }
            else if (ControlFreak2.CF2Input.GetButtonDown("Inventory") && open)
            {
                open = false;
                manager.Hide(this);
            }
        }
    }

}
