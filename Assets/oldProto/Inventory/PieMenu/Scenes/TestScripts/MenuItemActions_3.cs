using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuItemActions_3 : MonoBehaviour {

    public GameObject inventoryUI;  // The entire UI
    public GameObject cellCall;

    Scene m_Scene;
    string sceneName;

    void Awake()
    {
        inventoryUI = GameObject.Find("Canvas").transform.Find("Inventory").gameObject;

        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        if (sceneName == "07a Main Hub" || sceneName == "09 Scene 01")
        {
            cellCall = GameObject.Find("Cell_Urgence").transform.Find("Docteur").gameObject;
        }
        else if (sceneName == "14 Interstice")
        {
            cellCall = GameObject.Find("Cell_Urgence").transform.Find("NoDimentionSignal").gameObject;
        }
        else
        {
            cellCall = GameObject.Find("Cell_Urgence").transform.Find("NoSignal").gameObject;
        }
    }

    void OnSelect(string command) {
		Debug.Log("A Left Menu Command Received: " + command);
        if (command == "Inventaire")
        {
            inventoryUI.SetActive(!inventoryUI.activeSelf);
        }
        if (command == "Cell")
        {
            cellCall.SetActive(!cellCall.activeSelf);
        }
    }
	
}
