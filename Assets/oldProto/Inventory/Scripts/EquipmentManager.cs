﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Keep track of equipment. Has functions for adding and removing items. */

public class EquipmentManager : MonoBehaviour
{

    #region Singleton

    public Equipment[] defaultEquipment;

    public static EquipmentManager instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    Equipment[] currentEquipment;   // Items we currently have equipped

    Inventory inventory;    // Reference to our inventory

    void Start()
    {
        inventory = Inventory.instance;     // Get a reference to our inventory

        // Initialize currentEquipment based on number of equipment slots
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }

    // Equip a new item
    public void Equip(Equipment newItem)
    {
        // Find out what slot the item fits in
        int slotIndex = (int)newItem.equipSlot;

        Equipment oldItem = null;

        if (currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            inventory.AddItem(oldItem);
        }

        // Insert the item into the slot
        currentEquipment[slotIndex] = newItem;
    }

    // Unequip an item with a particular index
    public Equipment Unequip(int slotIndex)
    {
        Equipment oldItem = null;
        // Only do this if an item is there
        if (currentEquipment[slotIndex] != null)
        {
            // Add the item to the inventory
            oldItem = currentEquipment[slotIndex];
            inventory.AddItem(oldItem);

            // Remove the item from the equipment array
            currentEquipment[slotIndex] = null;
        }
        return oldItem;
    }

    // Unequip all items
    public void UnequipAll()
    {
        for (int i = 0; i < currentEquipment.Length; i++)
        {
            Unequip(i);
        }
    }

    void Update()
    {
        // Unequip all items if we press U
//        if (Input.GetKeyDown(KeyCode.U))
//            UnequipAll();
    }
}
