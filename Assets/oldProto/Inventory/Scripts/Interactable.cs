﻿using UnityEngine;

/*	
	This component is for all objects that the player can
	interact with such as enemies, items etc. It is meant
	to be used as a base class.
*/

public class Interactable : MonoBehaviour {

    public float radius = 3f;               // How close do we need to be to interact?
    public Transform interactionTransform;  // The transform from where we interact in case you want to offset it
//    public Transform player;       // Reference to the player transform

    bool canInteract = true; // Have we already interacted with the object?    set it back to false once the distance prob has been solved.

    public virtual void Interact()
    {
        // This method is meant to be overwritten
        Debug.Log("Interacting with " + transform.name);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player_girl(Clone)" || other.name == "Player_boy(Clone)" || other.name == "Player_girl" || other.name == "Player_boy" || other.name == "Player_vegan(Clone)" || other.name == "Player_vegan")
        {
            canInteract = true;
        }
    }

    void OnMouseDown()
    {
        if (canInteract)
        {
            // Interact with the object
            Interact();
            canInteract = false;
        }
    }

    // Draw our radius in the editor
    void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
            interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
