﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public static Inventory instance;

    // Use this for initialization
    void Awake () {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    // Callback which is triggered when an item gets added/removed.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 25;  // Amount of slots in inventory

    // Current list of items in inventory
    public List<Item> items = new List<Item>();

    // Add a new item. If there is enough room we return true. Else we return false.
    public bool AddItem(Item item)
    {
        // Don't do anything if it's a default item
        if (!item.isDefaultItem)
        {
            IsFull();  // check if inventory is full

            items.Add(item);    // Add item to list

            // Trigger callback
            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }

        return true;
    }

    // Remove an item
    public void Remove(Item item)
    {
        items.Remove(item);     // Remove item from list

        // Trigger callback
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

    public bool IsFull()
    {
        // Check if out of space
        if (items.Count >= space)
        {
            Debug.Log("Not enough room.");
            return false;
        }
        return true;
    }

    public bool Contains(Item item)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].name == item.name)
            {
                return true;
            }
        }
        return false;
    }

    public int ItemCount(Item item)
    {
        int number = 0;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].name == item.name)
            {
                number++;
            }
        }
        return number;
    }
}
