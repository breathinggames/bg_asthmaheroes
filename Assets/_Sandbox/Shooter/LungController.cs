﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungController : MonoBehaviour
{
    Rigidbody2D rb;
    Vector2 dirMove;
    bool isJumping;
    bool isGrounded;
    public float moveSpeed = 5f;
    public float jumpForce = 10f;

    private float boundaryX = -16f; // The x-position of the boundary

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isGrounded = true; // Assuming the player starts on the ground
    }

    void Update()
    {
        Vector3 playerPosition = transform.position;
        dirMove = new Vector2(Input.GetAxis("Horizontal"), 0);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Jump();
        }

        if (playerPosition.x < boundaryX)
        {
            playerPosition.x = boundaryX;
            transform.position = playerPosition;
        }
    }

    void FixedUpdate()
    {
        MoveCharacter(dirMove.x * moveSpeed);
    }

    void MoveCharacter(float horizontalMove)
    {
        Vector2 move = new Vector2(horizontalMove, rb.velocity.y);
        rb.velocity = move;

        if (move.x < 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (move.x > 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        isJumping = true;
        isGrounded = false;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            isJumping = false;
        }
    }
}
