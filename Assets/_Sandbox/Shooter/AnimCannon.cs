﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Fungus;

public class AnimCannon : MonoBehaviour
{

    public PlayableDirector playableDirector;
    public Flowchart flowchartRef;
    public GameObject playerController;
    public GameObject playerCam;

    void Start()
    {
        Deactivator();
        playableDirector.stopped += OnTimelineFinished;
    }

    void OnTimelineFinished(PlayableDirector director)
    {
        flowchartRef.ExecuteBlock("LoadAsteroid");
    }

    void OnDestroy()
    {
        playableDirector.stopped -= OnTimelineFinished;
    }

    public void PlayAnimClip()
    {
        playableDirector.Play();
    }

    public void Activator()
    {
        playerController.GetComponent<LungController>().enabled = true;
        playerCam.GetComponent<GetCamera>().enabled = true;
    }

    public void Deactivator()
    {
        playerController.GetComponent<LungController>().enabled = false;
        playerCam.GetComponent<GetCamera>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            flowchartRef.ExecuteBlock("Timeline");
        }
    }
}
