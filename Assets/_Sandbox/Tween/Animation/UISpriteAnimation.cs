﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISpriteAnimation : MonoBehaviour
{
    public Image image;
    public Sprite[] spriteArray;
    public float speed = .02f;

    private int indexSprite;
    private Coroutine coroutineSprite;
    private bool isDone;

    public void PlayUIAnimation()
    {
        isDone = false;
        StartCoroutine(Play());
    }

    IEnumerator Play()
    {
        yield return new WaitForSeconds(speed);
        if (indexSprite >= spriteArray.Length)
        {
            indexSprite = 0;
        }

        image.sprite = spriteArray[indexSprite];
        indexSprite++;
        if (isDone == false)
        {
            coroutineSprite = StartCoroutine(Play());
        }
    }

    public void Stop()
    {
        isDone = true;
        StopCoroutine(Play());
    }
}
