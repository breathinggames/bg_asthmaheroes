﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OrderCompleted : MonoBehaviour
{
    public void OrderResult()
    {
        if ((GameObject.Find("StepContainer 1").transform.position.y > GameObject.Find("StepContainer 2").transform.position.y) && (GameObject.Find("StepContainer 1").transform.position.x < GameObject.Find("StepContainer 3").transform.position.x) &&
            (GameObject.Find("StepContainer 2").transform.position.y < GameObject.Find("StepContainer 1").transform.position.y) && (GameObject.Find("StepContainer 2").transform.position.x < GameObject.Find("StepContainer 4").transform.position.x) &&
            (GameObject.Find("StepContainer 3").transform.position.y > GameObject.Find("StepContainer 4").transform.position.y) && (GameObject.Find("StepContainer 3").transform.position.x > GameObject.Find("StepContainer 1").transform.position.x) &&
            (GameObject.Find("StepContainer 4").transform.position.y < GameObject.Find("StepContainer 3").transform.position.y) && (GameObject.Find("StepContainer 4").transform.position.x > GameObject.Find("StepContainer 2").transform.position.x))
        {
            Debug.Log("Completed!");
            SceneManager.LoadScene("07c-6 House OrderEnd");
        }
        else
        {
            Debug.Log("Not Completed!");
        }
    }
}