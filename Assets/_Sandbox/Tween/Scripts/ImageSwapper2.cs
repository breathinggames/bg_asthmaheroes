using UnityEngine;
using UnityEngine.EventSystems;

public class ImageSwapper2 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform rectTransform;
    private Vector2 initialPosition;
    private Vector2 startPosition; // Store the start position of the drag.
    private SwapManager2 swapManager;
    private Canvas canvas;

    private bool isDragging = false;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        initialPosition = rectTransform.anchoredPosition;
        swapManager = FindObjectOfType<SwapManager2>();
        canvas = GetComponentInParent<Canvas>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = rectTransform.anchoredPosition; // Store the start position.
        swapManager.OnImageDragged(this);
        isDragging = true;

        // Set the dragged image to be the first child in the hierarchy
        rectTransform.SetAsFirstSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isDragging)
        {
            Vector2 localCursor;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                canvas.transform as RectTransform, eventData.position, eventData.pressEventCamera, out localCursor))
            {
                rectTransform.localPosition = localCursor;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        swapManager.OnImageDropped(this);

        // Check if the dragged image is dropped onto a swappable object.
        ImageSwapper targetImageSwapper = eventData.pointerEnter != null ?
            eventData.pointerEnter.GetComponent<ImageSwapper>() : null;

        if (targetImageSwapper != null && targetImageSwapper != this)
        {
            SwapPositions(targetImageSwapper);
        }
        else
        {
            // Return the dragged image to its initial position.
            ResetToInitialPosition();
        }

        // Restore the hierarchy order
        ResetHierarchyOrder();
    }

    private void SwapPositions(ImageSwapper targetSwapper)
    {
        Vector2 tempPosition = targetSwapper.GetInitialPosition();
        targetSwapper.SetInitialPosition(initialPosition);
        SetInitialPosition(tempPosition);
        targetSwapper.ResetToInitialPosition(); // Reset the target image as well
    }

    public Vector2 GetInitialPosition()
    {
        return initialPosition;
    }

    public void SetInitialPosition(Vector2 position)
    {
        initialPosition = position;
    }

    public void ResetToInitialPosition()
    {
        rectTransform.anchoredPosition = initialPosition;
    }

    private void ResetHierarchyOrder()
    {
        // Reset the hierarchy order to its original state
        transform.SetAsLastSibling();
    }
}
