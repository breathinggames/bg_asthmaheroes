using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapManager2 : MonoBehaviour
{
    private List<ImageSwapper2> imageSwappers = new List<ImageSwapper2>();
    private ImageSwapper2 draggedImage;

    public void RegisterImageSwapper(ImageSwapper2 swapper)
    {
        imageSwappers.Add(swapper);
    }

    public void OnImageDragged(ImageSwapper2 swapper)
    {
        draggedImage = swapper;
    }

    public void OnImageDropped(ImageSwapper2 swapper)
    {
        if (draggedImage != null && draggedImage != swapper)
        {
            Vector2 tempPosition = swapper.GetInitialPosition();
            swapper.SetInitialPosition(draggedImage.GetInitialPosition());
            draggedImage.SetInitialPosition(tempPosition);

            draggedImage = null;
        }
    }
}
