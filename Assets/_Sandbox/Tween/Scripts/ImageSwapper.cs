using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ImageSwapper : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform rectTransform;
    private Vector2 initialPosition;
    private Vector2 startPosition; // Store the start position of the drag.
    private SwapManager swapManager;
    private Canvas canvas;
    public Texture2D cursorTextureGrab;
    public Texture2D defaultHand;

    private bool isDragging = false;

    private void Start()
    {
        Cursor.SetCursor(defaultHand, Vector2.zero, CursorMode.Auto);
        rectTransform = GetComponent<RectTransform>();
        initialPosition = rectTransform.anchoredPosition;
        swapManager = FindObjectOfType<SwapManager>();
        canvas = GetComponentInParent<Canvas>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetAsFirstSibling();
        Cursor.SetCursor(cursorTextureGrab, Vector2.zero, CursorMode.Auto);
        startPosition = rectTransform.anchoredPosition; // Store the start position.
        swapManager.OnImageDragged(this);
        isDragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isDragging)
        {
            Vector2 localCursor;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, eventData.position, eventData.pressEventCamera, out localCursor))
            {
                localCursor.y += 534;
                rectTransform.localPosition = localCursor;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Cursor.SetCursor(defaultHand, Vector2.zero, CursorMode.Auto);
        isDragging = false;
        swapManager.OnImageDropped(this);

        // Check if the dragged image is dropped onto a swappable object.
        ImageSwapper targetImageSwapper = eventData.pointerEnter != null ?
            eventData.pointerEnter.GetComponent<ImageSwapper>() : null;

        if (targetImageSwapper != null && targetImageSwapper != this)
        {
            SwapPositions(targetImageSwapper);
        }
        else
        {
            // Return the dragged image to its initial position.
            ResetToInitialPosition();
        }

        // Restore the hierarchy order
        ResetHierarchyOrder();
    }

    private void SwapPositions(ImageSwapper targetSwapper)
    {
        Vector2 targetInitialPosition = targetSwapper.GetInitialPosition();

        // Use DoTween to animate the swapping smoothly
        Sequence sequence = DOTween.Sequence();

        // Move the dragged image to the target's initial position
        sequence.Append(rectTransform.DOAnchorPos(targetInitialPosition, 0.5f).SetEase(Ease.OutCubic));

        // Move the target image to the dragged image's initial position
        sequence.Join(targetSwapper.rectTransform.DOAnchorPos(initialPosition, 0.5f).SetEase(Ease.OutCubic));

        // Swap the initial positions of the two objects
        targetSwapper.SetInitialPosition(initialPosition);
        SetInitialPosition(targetInitialPosition);

        // Reset the target image as well
        targetSwapper.ResetToInitialPosition();
    }

    public Vector2 GetInitialPosition()
    {
        return initialPosition;
    }

    public void SetInitialPosition(Vector2 position)
    {
        initialPosition = position;
    }

    public void ResetToInitialPosition()
    {
        rectTransform.anchoredPosition = initialPosition;
    }

    private void ResetHierarchyOrder()
    {
        // Reset the hierarchy order to its original state
        transform.SetAsLastSibling();
    }
}
