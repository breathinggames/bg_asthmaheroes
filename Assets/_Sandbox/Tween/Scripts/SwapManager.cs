using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwapManager : MonoBehaviour
{
    private List<ImageSwapper> imageSwappers = new List<ImageSwapper>();
    private ImageSwapper draggedImage;

    public void RegisterImageSwapper(ImageSwapper swapper)
    {
        imageSwappers.Add(swapper);
    }

    public void OnImageDragged(ImageSwapper swapper)
    {
        draggedImage = swapper;
    }

    public void OnImageDropped(ImageSwapper swapper)
    {
        if (draggedImage != null && draggedImage != swapper)
        {
            Vector2 tempPosition = swapper.GetInitialPosition();
            swapper.SetInitialPosition(draggedImage.GetInitialPosition());
            draggedImage.SetInitialPosition(tempPosition);

            draggedImage = null;
        }
    }
}
