using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveImage : MonoBehaviour
{
    [SerializeField] private Transform[] positions;
    [SerializeField] private float objectSpeed;

    int nextPosIndex;
    Transform nextPos;

    void Start()
    {
        nextPos = positions[0];
        
        // MoveWithTween();
    }

    private void Update()
    {
        MoveGameObjectWithoutTween();
    }

    private void MoveGameObjectWithoutTween()
    {
        if (transform.position == nextPos.position)
        {
            nextPosIndex++;
            if (nextPosIndex >= positions.Length)
            {
                nextPosIndex = 0;
            }

            nextPos = positions[nextPosIndex];
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPos.position, objectSpeed * Time.deltaTime);
        }
    }

    private void MoveWithTween()
    {
        transform.DOMoveY(3.0f, 2.0f, true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
    }
}
