﻿using AH_Galaga;
using Fungus;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PEP_Invader
{
    public sealed class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        [SerializeField] private Text scoreText;
        [SerializeField] private Text livesText;

        private Player player;
        private Invaders invaders;
        private MysteryShip mysteryShip;
        private Bunker[] bunkers;
        private bool isDone;

        private int score;
        private int lives;

        public int Score => score;
        public int Lives => lives;

        public Flowchart flowchartRef;
        public PlayableDirector playableDirector;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            player = FindObjectOfType<Player>();
            invaders = FindObjectOfType<Invaders>();
            mysteryShip = FindObjectOfType<MysteryShip>();
            bunkers = FindObjectsOfType<Bunker>();

            playableDirector.stopped += OnTimelineFinished;

            NewGame();
        }

        void OnTimelineFinished(PlayableDirector director)
        {
            flowchartRef.ExecuteBlock("LoadScene");
        }

        void OnDestroy()
        {
            playableDirector.stopped -= OnTimelineFinished;
        }

        public void PlayAnimClip()
        {
            playableDirector.Play();
        }

        private void Update()
        {
            if (!isDone && int.Parse(scoreText.text) >= 500)
            {
                player.GetComponent<Player>().enabled = false;
                player.GetComponent<BoxCollider2D>().enabled = false;
             //   GameOver();
                flowchartRef.ExecuteBlock("End");
                isDone = true;
            }

            if (lives <= 0)
            {
                ReloadScene();
            }
        }

        public void ReloadScene()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.buildIndex);
        }

        private void NewGame()
        {
            SetScore(0);
            SetLives(3);
            NewRound();
        }

        private void NewRound()
        {
            invaders.ResetInvaders();
            invaders.gameObject.SetActive(true);

            for (int i = 0; i < bunkers.Length; i++)
            {
                bunkers[i].ResetBunker();
            }

            Respawn();
        }

        private void Respawn()
        {
            Vector3 position = player.transform.position;
            position.x = 0f;
            player.transform.position = position;
            player.gameObject.SetActive(true);
        }

        private void GameOver()
        {
            invaders.gameObject.SetActive(false);
        }

        private void SetScore(int score)
        {
            this.score = score;
            scoreText.text = score.ToString().PadLeft(4, '0');
        }

        private void SetLives(int lives)
        {
            this.lives = Mathf.Max(lives, 0);
            livesText.text = this.lives.ToString();
        }

        public void OnPlayerKilled(Player player)
        {
            SetLives(lives - 1);

            player.gameObject.SetActive(false);

            if (lives > 0)
            {
                 Invoke(nameof(Respawn), 1f);
            }
            //else
            //{
            //    GameOver();
            //}
        }

        public void OnInvaderKilled(Invader invader)
        {
            invader.gameObject.SetActive(false);

            SetScore(score + invader.score);

            if (invaders.GetAliveCount() == 0)
            {
             //   NewRound();
            }
        }

        public void OnBoundaryReached()
        {
            if (invaders.gameObject.activeSelf)
            {
                invaders.gameObject.SetActive(false);

                OnPlayerKilled(player);
            }
        }

    }
}