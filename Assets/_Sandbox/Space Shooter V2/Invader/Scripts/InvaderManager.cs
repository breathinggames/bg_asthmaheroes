﻿using PEP_Invader;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class InvaderManager : MonoBehaviour
{
    public Player player;

    private void Start()
    {
        Deactivator();
    }

    public void Activator()
    {
        player.GetComponent<Player>().enabled = true;
    }

    public void Deactivator()
    {
        player.GetComponent<Player>().enabled = false;
    }
}
