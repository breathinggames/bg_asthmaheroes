﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Fungus;
using AH_Galaga;
using UnityEngine.Playables;

public class LungLauncherUIManager : MonoBehaviour
{
    public static LungLauncherUIManager instance;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI lifeText;
    public TextMeshProUGUI stageText;

    public Flowchart flowchartRef;
    public LungPlayerBehavior player;
    public PlayableDirector playableDirector;

    private bool isDone;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Deactivator();
        playableDirector.stopped += OnTimelineFinished;
    }

    void OnTimelineFinished(PlayableDirector director)
    {
        flowchartRef.ExecuteBlock("LoadScene");
    }

    void OnDestroy()
    {
        playableDirector.stopped -= OnTimelineFinished;
    }

    public void PlayAnimClip()
    {
        playableDirector.Play();
    }

    private void Update()
    {
        if (!isDone && int.Parse(scoreText.text) >= 1000)
        {
            player.GetComponent<LungPlayerBehavior>().enabled = false;
            player.GetComponent<SphereCollider>().enabled = false;
            flowchartRef.ExecuteBlock("End");
            isDone = true;
        }
    }

    public void UpdateScoreText(int amount)
    {
        scoreText.text = amount.ToString("D9");
    }

    public void UpdateLifeText(int amount)
    {
        lifeText.text = "x" + amount.ToString("D2");
    }

    public void UpdateStageText(int amount)
    {
      //  stageText.gameObject.SetActive(true);
      //  stageText.text = amount.ToString("D1");

     //   Invoke("DeactivateStageText", 3f);
    }

    private void DeactivateStageText()
    {
        stageText.gameObject.SetActive(false);
        CancelInvoke("DeactivateStageText");
    }


    public void Activator()
    {
        player.GetComponent<LungPlayerBehavior>().enabled = true;
    }

    public void Deactivator()
    {
        player.GetComponent<LungPlayerBehavior>().enabled = false;
    }
}
