﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AH_Galaga
{
    public class SpawnManager : MonoBehaviour
    {
        [Header("Intervals")]
        public float enemySpawnInterval;
        public float waveSpawnInterval;

        [Header("Trigger Enemy Prefabs")]
        public GameObject catTriggerPrefab;
        public GameObject cigaretteTriggerPrefab;
        public GameObject dumbellTriggerPrefab;

        private int currentWave;

        [Header("Trigger Enemy ID")]
        private int lowerFormationID = 0;
        private int middleFormationID = 0;
        private int upperFormationID = 0;

        [Header("Trigger Enemy Formation")]
        public TriggerFormation lowerFormation;
        public TriggerFormation middleFormation;
        public TriggerFormation upperFormation;

        [System.Serializable]
        public class Wave
        {
            public int catAmountPerWave;
            public int cigaretteAmountPerWave;
            public int dumbellAmountPerWave;

            public GameObject[] pathPrefabs;
        }

        [Header("Waves")]
        public List<Wave> waves = new List<Wave>();
        public List<GameObject> spawnedEnemyTriggers = new List<GameObject>();
        private List<PathSystem> activePathList = new List<PathSystem>();

        private bool spawnComplete;

        void Start()
        {
            Invoke("StartSpawn", 3f);
        }

        void OnValidate()
        {
            // debug for cat trigger
            int currentCatAmount = 0;
            for (int i = 0; i < waves.Count; i++)
            {
                currentCatAmount += waves[i].catAmountPerWave;
            }
            
            if (currentCatAmount > 20)
            {
                Debug.LogError("<color=red>Error!!</color> Cat amount is too high! " + currentCatAmount + "/20");
            }
            else
            {
                Debug.Log("Current Cat amount: " + currentCatAmount);
            }

            // add other triggers debug here
        }

        IEnumerator SpawnWaves()
        {
            while (currentWave < waves.Count)
            {
                if (currentWave == waves.Count - 1)
                {
                    spawnComplete = true; 
                }

                for (int i = 0; i < waves[currentWave].pathPrefabs.Length; i++)
                {
                    GameObject newPathObj = Instantiate(waves[currentWave].pathPrefabs[i], transform.position, Quaternion.identity) as GameObject;
                    PathSystem newPath = newPathObj.GetComponent<PathSystem>();
                    activePathList.Add(newPath);
                }
                
                // Cat Trigger
                for (int i = 0; i < waves[currentWave].catAmountPerWave; i++)
                {
                    GameObject newCatTrigger = Instantiate(catTriggerPrefab, transform.position, Quaternion.identity) as GameObject;
                    EnemyAIBehavior triggerCatBehaviour = newCatTrigger.GetComponent<EnemyAIBehavior>();

                    triggerCatBehaviour.SpawnSetup(activePathList[PathPingPong()], lowerFormationID, lowerFormation);
                    lowerFormationID++;

                    spawnedEnemyTriggers.Add(newCatTrigger);

                    yield return new WaitForSeconds(enemySpawnInterval);
                }

                // Cigarette Trigger
                for (int i = 0; i < waves[currentWave].cigaretteAmountPerWave; i++)
                {
                    GameObject newCigaretteTrigger = Instantiate(cigaretteTriggerPrefab, transform.position, Quaternion.identity) as GameObject;
                    EnemyAIBehavior triggerCigaretteBehaviour = newCigaretteTrigger.GetComponent<EnemyAIBehavior>();

                    triggerCigaretteBehaviour.SpawnSetup(activePathList[PathPingPong()], lowerFormationID, lowerFormation);
                    lowerFormationID++;

                    spawnedEnemyTriggers.Add(newCigaretteTrigger);

                    yield return new WaitForSeconds(enemySpawnInterval);
                }

                // Dumbell Trigger
                for (int i = 0; i < waves[currentWave].dumbellAmountPerWave; i++)
                {
                    GameObject newDumbellTrigger = Instantiate(dumbellTriggerPrefab, transform.position, Quaternion.identity) as GameObject;
                    EnemyAIBehavior triggerDumbellBehaviour = newDumbellTrigger.GetComponent<EnemyAIBehavior>();

                    triggerDumbellBehaviour.SpawnSetup(activePathList[PathPingPong()], lowerFormationID, lowerFormation);
                    lowerFormationID++;

                    spawnedEnemyTriggers.Add(newDumbellTrigger);

                    yield return new WaitForSeconds(enemySpawnInterval);
                }

                yield return new WaitForSeconds(waveSpawnInterval);
                currentWave++;

                foreach (PathSystem p in activePathList)
                {
                    Destroy(p.gameObject);
                }

                activePathList.Clear();
            }

            Invoke("CheckTriggerEnemyState", 1f);
        }

        private void CheckTriggerEnemyState()
        {
            bool information = false;

            for (int i = spawnedEnemyTriggers.Count - 1; i >= 0 ; i--)
            {
                if (spawnedEnemyTriggers[i].GetComponent<EnemyAIBehavior>().enemyState != EnemyAIBehavior.EnemyStates.IDLE)
                {
                    information = false;
                    Invoke("CheckTriggerEnemyState", 1f);
                    break;
                }
            }

            information = true;

            if (information)
            {
                StartCoroutine(lowerFormation.ActivateSpread());
                StartCoroutine(middleFormation.ActivateSpread());
                StartCoroutine(upperFormation.ActivateSpread());

                CancelInvoke("CheckTriggerEnemyState");
            }
        }

        void StartSpawn()
        {
            StartCoroutine(SpawnWaves());
            CancelInvoke("StartSpawn");
        }

        private int PathPingPong()
        {
            return (lowerFormationID + middleFormationID + upperFormationID) % activePathList.Count;
        }

        private void ReportToGameManager()
        {
            if (spawnedEnemyTriggers.Count == 0 && spawnComplete)
            {
                // AHGManager.instance.Wincondition();
            }
        }

        public void UpdateSpawnedEnemies(GameObject enemy)
        {
            spawnedEnemyTriggers.Remove(enemy);
            ReportToGameManager();
        }
    }
}