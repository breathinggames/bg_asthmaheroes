﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AH_Galaga
{
    public class AHGManager : MonoBehaviour
    {
        public static AHGManager instance;

        static int level = 1;
        static int score;
        static int bonusScore;
        static int life = 3;
        static bool hasLost;

        int enemyAmount;
        int bonusLife = 10000;

        private void Awake()
        {
            instance = this;

            if (hasLost)
            {
                level = 1;
                score = 0;
                life = 3;
                bonusScore = 0;
                hasLost = false;
            }
        }

        private void Start()
        {
            LungLauncherUIManager.instance.UpdateScoreText(score);
            LungLauncherUIManager.instance.UpdateLifeText(life);
            LungLauncherUIManager.instance.UpdateStageText(level);
        }

        public void AddScore(int amount)
        {
            score += amount;
            LungLauncherUIManager.instance.UpdateScoreText(score);

            bonusScore += amount;
            if (bonusScore >= bonusLife)
            {
                life++;
                bonusScore %= bonusLife;
            }
        }

        public void DecreaseLife()
        {
            life--;
            LungLauncherUIManager.instance.UpdateLifeText(life);

            if (life <= 0 )
            {
                ScoreHolder.score = score;
                ScoreHolder.level = level;
                hasLost = true;

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        public void Wincondition()
        {
            level++;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}