﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace AH_Galaga
{
    public class TriggerFormation : MonoBehaviour
    {
        public float gridSizeX = 10;
        public float gridSizeY = 2;
               
        public float gridOffsetX = 1f;
        public float gridOffsetY = 1f;

        public int div = 4;

        public List<Vector3> gridList = new List<Vector3>();

        // move the formation
        public float maxMoveOffsetX = 5f;
        public float speed = 1f;

        private float currentPosX;
        private Vector3 startPos;
        private int direction = -1;

        // Spreading behavior
        private bool canSpread;
        private bool spreadStarted;
        private float spreadAmount = 1f;
        private float spreadSpeed = 0.5f;
        private int spreadDirection = 1;
        private float currentSpread;

        // Diving behavior
        private bool canDive;
        public List<GameObject> divePathList = new List<GameObject>();

        public List<Formation> formations = new List<Formation>();

        [System.Serializable]
        public class Formation
        {
            public int index;
            public float xPos;
            public float yPos;
            public GameObject enemyTrigger;
            public Vector3 goal;
            public Vector3 start;

            public Formation(int index, float xPos, float yPos, GameObject enemyTrigger)
            {
                this.index = index;
                this.xPos = xPos;
                this.yPos = yPos;
                this.enemyTrigger = enemyTrigger;

                start = new Vector3(xPos, yPos, 0);
                goal = new Vector3(xPos + (xPos * 0.3f), yPos, 0);
            }
        }

        private void Start()
        {
            startPos = transform.position;
            currentPosX = transform.position.x;

            CreateGrid();
        }

        private void Update()
        {
            if (!canSpread && !spreadStarted)
            {
                currentPosX += Time.deltaTime * speed * direction;
                if (currentPosX >= maxMoveOffsetX)
                {
                    direction *= -1;
                    currentPosX = maxMoveOffsetX;
                }
                else if (currentPosX <= -maxMoveOffsetX)
                {
                    direction *= -1;
                    currentPosX = -maxMoveOffsetX;
                }

                transform.position = new Vector3(currentPosX, startPos.y, startPos.z);
            }

            if (canSpread)
            {
                currentSpread += Time.deltaTime * spreadDirection * spreadSpeed;
                if (currentSpread >= spreadAmount || currentSpread <= 0)
                {
                    // change spread direction
                    spreadDirection *= -1;
                }

                for (int i = 0; i < formations.Count; i++)
                {
                    if (Vector3.Distance(formations[i].enemyTrigger.transform.position, formations[i].goal) >= 0.001f)
                    {
                        formations[i].enemyTrigger.transform.position = Vector3.Lerp(transform.position + formations[i].start, transform.position + formations[i].goal, currentSpread);
                    }
                }
            }
        }

        public IEnumerator ActivateSpread()
        {
            if (spreadStarted)
            {
                yield break;
            }

            spreadStarted = true;

            while (transform.position.x != startPos.x) 
            {
                transform.position = Vector3.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
                yield return null;
            }

            canSpread = true;
            Invoke("SetDiving", Random.Range(3, 10));
        }

        /*
        private void OnDrawGizmos()
        {
            gridList.Clear();

            int num = 0;

            for (int i = 0; i < gridSizeX; i++) 
            { 
                for (int j = 0; j < gridSizeY; j++)
                {
                    float x = (gridOffsetX + gridOffsetX * 2 * (num / div)) * Mathf.Pow( -1, num % 2 + 1);
                    float y = gridOffsetY * ((num % div) / 2);

                    Vector3 v = new Vector3(transform.position.x + x, transform.position.y + y, 0);

                    // visualize position
                    Handles.Label(v, num.ToString());
                    num++;

                    gridList.Add(v);
                }
            }
        }

        private void OnDrawGizmos()
        {
            int num = 0;

            CreateGrid();
            foreach (Vector3 pos in gridList)
            {
                Gizmos.DrawWireSphere(GetVector(num), 0.1f);
                num++;
            }
        }

        */

        private void CreateGrid()
        {
            gridList.Clear();

            int num = 0;

            for (int i = 0; i < gridSizeX; i++)
            {
                for (int j = 0; j < gridSizeY; j++)
                {
                    float x = (gridOffsetX + gridOffsetX * 2 * (num / div)) * Mathf.Pow(-1, num % 2 + 1);
                    float y = gridOffsetY * ((num % div) / 2);

                    Vector3 v = new Vector3(x, y, 0);
                    num++;

                    gridList.Add(v);
                }
            }
        }

        private void SetDiving()
        {
            if (formations.Count > 0)
            {
                int choosenPath = Random.Range(0, divePathList.Count);
                int choosenFormation = Random.Range(0, formations.Count);

                GameObject newPath = Instantiate(divePathList[choosenPath], formations[choosenFormation].start + transform.position, Quaternion.identity);
                formations[choosenFormation].enemyTrigger.GetComponent<EnemyAIBehavior>().DiveSetup(newPath.GetComponent<PathSystem>());
                formations.RemoveAt(choosenFormation);
                Invoke("SetDiving", Random.Range(3, 10));
            }
            else
            {
                CancelInvoke("SetDiving");
                return;
            }
        }

        public Vector3 GetVector(int ID)
        {
            return transform.position + gridList[ID];
        }
    }
}