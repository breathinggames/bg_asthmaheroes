﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AH_Galaga
{
    public class EnemyAIBehavior : MonoBehaviour
    {
        public PathSystem pathToFollow;
        public int currentWaypointID = 0;
        public float speed = 2;
        public float reachDistance = 0.4f;
        public float rotationSpeed = 5f;
        public bool useBezier;

        // current distance to next waypoint
        private float distance;

        // state machine handling
        public enum EnemyStates
        {
            ON_PATH,
            FLY_IN,
            IDLE,
            DIVE
        }

        public EnemyStates enemyState;
        public int enemyID;
        public TriggerFormation triggerFormation;

        // Health
        public int health = 1;

        // Shooting
        public GameObject projectile;
        public Transform projectileSpawnPoint;
        public float currentDelay;
        public float fireRate = 2f;
        private Transform target;
        public int projectileDamage = -1;

        // score
        public int inFormationScore;
        public int inDivingScore;

        // trails
        public TrailRenderer[] trails;

        // effects
        public GameObject fxExplosion;

        private void Start()
        {
            target = GameObject.Find("LungCharacter").transform;
        }

        private void Update()
        {
            switch (enemyState)
            {
                case EnemyStates.ON_PATH:
                    TrailActivate(true);
                    MoveOnPath(pathToFollow);
                    break;
                case EnemyStates.FLY_IN:
                    MoveToFormation();
                    break;
                case EnemyStates.IDLE:
                    TrailActivate(false);
                    break;
                case EnemyStates.DIVE:
                    TrailActivate(true);
                    MoveOnPath(pathToFollow);
                    SpawnProjectile();
                    break;
                default:
                    break;
            }
        }

        private void MoveToFormation()
        {
            transform.position = Vector3.MoveTowards(transform.position, triggerFormation.GetVector(enemyID), speed * Time.deltaTime);

            // rotation on the curve to point forward
            Vector3 direction = triggerFormation.GetVector(enemyID) - transform.position;
            if (direction != Vector3.zero)
            {
                direction.y = 0;
                direction = direction.normalized;
                Quaternion rotation = Quaternion.LookRotation(direction);
                rotation.y = 0;
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
            }

            if (Vector3.Distance(transform.position, triggerFormation.GetVector(enemyID)) <= 0.0001f)
            {
                transform.SetParent(triggerFormation.gameObject.transform);
                transform.eulerAngles = Vector3.zero;

                triggerFormation.formations.Add(new TriggerFormation.Formation(enemyID, transform.localPosition.x, transform.localPosition.y, gameObject));

                enemyState = EnemyStates.IDLE;
            }
        }

        private void MoveOnPath(PathSystem path)
        {
            if (useBezier)
            {
                // moving the enemy
                distance = Vector2.Distance(path.bezierObjList[currentWaypointID], transform.position);
                transform.position = Vector2.MoveTowards(transform.position, path.bezierObjList[currentWaypointID], speed * Time.deltaTime);

                // rotation on the curve to point forward
                Vector3 direction = path.bezierObjList[currentWaypointID] - transform.position;
                if (direction != Vector3.zero)
                {
                    direction.y = 0;
                    direction = direction.normalized;
                    Quaternion rotation = Quaternion.LookRotation(direction);
                    rotation.y = 0;
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
                }
            }
            else
            {
                distance = Vector2.Distance(path.pathObjList[currentWaypointID].position, transform.position);
                transform.position = Vector2.MoveTowards(transform.position, path.pathObjList[currentWaypointID].position, speed * Time.deltaTime);
            }

            if (useBezier)
            {
                if (distance <= reachDistance)
                {
                    currentWaypointID++;
                }

                if (currentWaypointID >= path.bezierObjList.Count)
                {
                    currentWaypointID = 0;

                    // Diving
                    if (enemyState == EnemyStates.DIVE)
                    {
                        TrailActivate(false);
                        transform.position = GameObject.Find("SpawnManager").transform.position;
                        Destroy(pathToFollow.gameObject);
                    }

                    enemyState = EnemyStates.FLY_IN;
                }
            }
            else 
            {
                if (distance <= reachDistance)
                {
                    currentWaypointID++;
                }

                if (currentWaypointID >= path.pathObjList.Count)
                {
                    currentWaypointID = 0;

                    // Diving
                    if (enemyState == EnemyStates.DIVE)
                    {
                        TrailActivate(false);
                        transform.position = GameObject.Find("SpawnManager").transform.position;
                        Destroy(pathToFollow.gameObject);
                    }

                    enemyState = EnemyStates.FLY_IN;
                }
            }
        }

        public void SpawnSetup(PathSystem path, int ID, TriggerFormation formation)
        {
            pathToFollow = path;
            enemyID = ID;
            this.triggerFormation = formation;
        }

        public void DiveSetup(PathSystem path)
        {
            pathToFollow = path;
            transform.SetParent(transform.parent.parent);
            enemyState = EnemyStates.DIVE;
        }

        public void TakeDamage(int amount)
        {
            health -= amount;
            if (health <= 0)
            {
                if (fxExplosion != null)
                {
                    Instantiate(fxExplosion, transform.position, Quaternion.identity); 
                }

                if (enemyState == EnemyStates.IDLE)
                {
                    AHGManager.instance.AddScore(inFormationScore);
                }
                else
                {
                    AHGManager.instance.AddScore(inDivingScore);
                }

                // report to formation
                for (int i = 0; i < triggerFormation.formations.Count; i++)
                {
                    if (triggerFormation.formations[i].index == enemyID)
                    {
                        triggerFormation.formations.Remove(triggerFormation.formations[i]);
                    }
                }

                // Report to the spawn manager
                SpawnManager sp = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

                sp.UpdateSpawnedEnemies(gameObject);
                Destroy(gameObject);
            }
        }

        private void SpawnProjectile()
        {
            currentDelay += Time.deltaTime;

            if (currentDelay >= fireRate && projectile != null && projectileSpawnPoint != null)
            {
                Debug.Log("Enemy Firing!");  // the projectile fires but we can't see it - probably caused by the 3d transform in a 2d space
                projectileSpawnPoint.LookAt(target);
                GameObject newProjectile = Instantiate(projectile, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
                newProjectile.GetComponent<LungProjectile>().SetDamage(projectileDamage);
                currentDelay = 0;
            }         
        }

        private void TrailActivate(bool state)
        {
            foreach (TrailRenderer trail in trails)
            {
                trail.enabled = state;
            }
        }
    }
}