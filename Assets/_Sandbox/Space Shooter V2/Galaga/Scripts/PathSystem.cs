﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AH_Galaga
{
    public class PathSystem : MonoBehaviour
    {
        public Color pathColor = Color.green;
        public bool VisualizePath;
        public List<Transform> pathObjList = new List<Transform>();
        [Range(1, 20)] public int lineDensity = 1;
        public List<Vector3> bezierObjList = new List<Vector3>();

        private Transform[] objArray;
        private int overload;

        private void Start()
        {
            CreatePath();
        }

        private void OnDrawGizmos()
        {
            if (VisualizePath)
            {
                // Straight path
                Gizmos.color = pathColor;

                // fill the array
                objArray = GetComponentsInChildren<Transform>();

                // clear list and add all children into the list
                pathObjList.Clear();
                foreach (Transform obj in objArray)
                {
                    if (obj != transform)
                    {
                        pathObjList.Add(obj);
                    }
                }

                // Draw objects
                for (int i = 0; i < pathObjList.Count; i++)
                {
                    Vector3 position = pathObjList[i].position;
                    if (i > 0)
                    {
                        Vector3 previous = pathObjList[i - 1].position;
                        Gizmos.DrawLine(previous, position);
                        Gizmos.DrawWireSphere(position, 0.3f);
                    }
                }

                // check for overload values
                if (pathObjList.Count % 2 == 0)
                {
                    pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                    overload = 2;
                }
                else
                {
                    pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                    pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                    overload = 3;
                }

                // bezier path creation
                bezierObjList.Clear();
                Vector3 lineStart = pathObjList[0].position;

                for (int i = 0; i < pathObjList.Count - overload; i += 2)
                {
                    for (int j = 0; j < lineDensity; j++)
                    {
                        Vector3 lineEnd = GetPoint(pathObjList[i].position, pathObjList[i + 1].position, pathObjList[i + 2].position, j / (float)lineDensity);

                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(lineStart, lineEnd);

                        Gizmos.color = Color.blue;
                        Gizmos.DrawWireSphere(lineStart, 0.1f);

                        lineStart = lineEnd;
                        bezierObjList.Add(lineStart);
                    }
                }
            }
            else
            {
                // pathObjList.Clear();
                // bezierObjList.Clear();
            }
        }

        private Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            return Vector3.Lerp(Vector3.Lerp(p0, p1, t), Vector3.Lerp(p1, p2, t), t);          
        }

        private void CreatePath()
        {
            // fill the array
            objArray = GetComponentsInChildren<Transform>();

            // clear list and add all children into the list
            pathObjList.Clear();
            foreach (Transform obj in objArray)
            {
                if (obj != transform)
                {
                    pathObjList.Add(obj);
                }
            }

            // check for overload values
            if (pathObjList.Count % 2 == 0)
            {
                pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                overload = 2;
            }
            else
            {
                pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                pathObjList.Add(pathObjList[pathObjList.Count - 1]);
                overload = 3;
            }

            // bezier path creation
            bezierObjList.Clear();
            Vector3 lineStart = pathObjList[0].position;

            for (int i = 0; i < pathObjList.Count - overload; i += 2)
            {
                for (int j = 0; j < lineDensity; j++)
                {
                    Vector3 lineEnd = GetPoint(pathObjList[i].position, pathObjList[i + 1].position, pathObjList[i + 2].position, j / (float)lineDensity);

                    lineStart = lineEnd;
                    bezierObjList.Add(lineStart);
                }
            }
        }
    }
}