﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AH_Galaga
{
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class LungProjectile : MonoBehaviour
    {
        private int damage;

        public float speed = 10f;
        public float destroyTimer = 2f;

        public enum Targets
        {
            Enemy,
            Player
        }

        public Targets target;

        public void Start()
        {
            Destroy(gameObject, destroyTimer);
        }

        void Update()
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }

        public void SetDamage(int amount)
        {
            damage = amount;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (target == Targets.Player)
            {
                if (other.tag == "Player")
                {
                    other.gameObject.GetComponent<LungPlayerBehavior>().TakeDamage(damage);
                    Destroy(gameObject);
                }
            }

            if (target == Targets.Enemy)
            {
                if (other.tag == "Enemy")
                {
                    other.gameObject.GetComponent<EnemyAIBehavior>().TakeDamage(damage);

                    // play sound - particle - score

                    Destroy(gameObject);
                }
            }

            if (other.tag == "Boundary")
            {
                // play sound - particle - score

                Destroy(gameObject);
            }
        }
    }
}