﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Fungus;
using UnityEngine.Playables;
using UnityEngine.Analytics;

namespace PEP_Asteroid
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        [SerializeField] private Player player;
        [SerializeField] private ParticleSystem explosionEffect;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text livesText;

        private int score;
        private int lives;
        private bool endIsDone;

        public int Score => score;
        public int Lives => lives;

        public Flowchart flowchartRef;
        public PlayableDirector timeline;

        public GameObject gameOver;

        private void Awake()
        {
            if (Instance != null)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Start()
        {
            timeline.stopped += OnTimelineFinished;
            NewGame();
        }

        private void Update()
        {
            if (lives <= 0 && ControlFreak2.CF2Input.GetKeyDown(KeyCode.Return))
            {
                NewGame();
            }

            EndScore();
        }

        void OnTimelineFinished(PlayableDirector director)
        {
            flowchartRef.ExecuteBlock("LoadScene");
        }

        void OnDestroy()
        {
            timeline.stopped -= OnTimelineFinished;
        }

        private void EndScore()
        {
            if (score >= 2000 && !endIsDone)
            {
                player.GetComponent<BoxCollider2D>().enabled = false;
                player.GetComponent<Player>().enabled = false;
                endIsDone = true;
                flowchartRef.ExecuteBlock("End");
            }
        }

        public void PlayAnimClip()
        {
            timeline.Play();
        }

        private void NewGame()
        {
            Asteroid[] asteroids = FindObjectsOfType<Asteroid>();

            for (int i = 0; i < asteroids.Length; i++)
            {
                Destroy(asteroids[i].gameObject);
            }

            SetScore(0);
            SetLives(3);
            Respawn();
        }

        private void SetScore(int score)
        {
            this.score = score;
            scoreText.text = score.ToString();
        }

        private void SetLives(int lives)
        {
            this.lives = lives;
            livesText.text = lives.ToString();
        }

        private void Respawn()
        {
            player.transform.position = Vector3.zero;
            player.gameObject.SetActive(true);
        }

        public void OnAsteroidDestroyed(Asteroid asteroid)
        {
            explosionEffect.transform.position = asteroid.transform.position;
            explosionEffect.Play();

            if (asteroid.size < 0.4f)
            {
                SetScore(score + 100); // small asteroid
            }
            else if (asteroid.size < 0.8f)
            {
                SetScore(score + 50); // medium asteroid
            }
            else
            {
                SetScore(score + 25); // large asteroid
            }
        }

        public void OnPlayerDeath(Player player)
        {
            player.gameObject.SetActive(false);

            explosionEffect.transform.position = player.transform.position;
            explosionEffect.Play();

            SetLives(lives - 1);

            if (lives <= 0)
            {
                gameOver.SetActive(true);
            }
            else
            {
                Invoke(nameof(Respawn), player.respawnDelay);
            }
        }

        public void ReloadScene()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.buildIndex);
        }
    }
}