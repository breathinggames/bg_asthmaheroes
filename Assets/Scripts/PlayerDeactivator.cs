﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class PlayerDeactivator : MonoBehaviour
    {
        void Update()
        {
            if (this.gameObject.activeInHierarchy)
            {
                if (GameObject.Find("Canvas").activeInHierarchy)
                {
                    GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
                    GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = false;
                }

                if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
                {
                    GameObject.Find("Scene").transform.Find("Player_girl(Clone)").gameObject.SetActive(false);
                }
            }
            else
            {
                return;
            }
        }
    }
}