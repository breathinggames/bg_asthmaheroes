﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDeactivator : MonoBehaviour
{
    private bool hasBeenDoneOnce;
    
    void Update()
    {
        if (!hasBeenDoneOnce)
        {
            DeactivatorInit();
            hasBeenDoneOnce = true;
        }
    }

    void DeactivatorInit()
    {
        Button[] childButtons =  GetComponentsInChildren<Button>();

        foreach (Button button in childButtons)
        {
            if (button.name != "OptionButton0")
            {
                button.interactable = false;
            }
        }
    }
}
