﻿using BreathingGames.Character;
using BreathingGames.UISystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogField : MonoBehaviour
{
    // when entering a Fog field
    private void OnTriggerEnter2D(Collider2D other)
    {
        //GameObject.Find("Canvas").GetComponent<PlayerUI>().isAllergenRegenActive = true;
    }

    // while being inside a Fog field
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (!GameObject.Find("Canvas").GetComponent<PlayerUI>().isMaskOn && GameObject.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().isRunning)
            {
                GameObject.Find("Canvas").GetComponent<PlayerUI>().lungCapacity.CurrentVal -= 0.02f;
            }
            else if(!GameObject.Find("Canvas").GetComponent<PlayerUI>().isMaskOn && !GameObject.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().isRunning)
            {
                GameObject.Find("Canvas").GetComponent<PlayerUI>().lungCapacity.CurrentVal -= 0.01f;
            }
            else if (GameObject.Find("Canvas").GetComponent<PlayerUI>().isMaskOn && GameObject.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().isRunning)
            {
                GameObject.Find("Canvas").GetComponent<PlayerUI>().lungCapacity.CurrentVal -= 0.005f;
            }
            else if (GameObject.Find("Canvas").GetComponent<PlayerUI>().isMaskOn && !GameObject.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().isRunning)
            {
                GameObject.Find("Canvas").GetComponent<PlayerUI>().lungCapacity.CurrentVal -= 0.001f;
            }
        }
    }

    // when exiting a Fog field
    private void OnTriggerExit2D(Collider2D other)
    {
        //GameObject.Find("Canvas").GetComponent<PlayerUI>().isAllergenRegenActive = false;
    }
}
