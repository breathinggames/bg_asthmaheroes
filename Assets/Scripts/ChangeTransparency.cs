﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace BreathingGames.Gameplay
{
    public class ChangeTransparency : MonoBehaviour
    {
        Scene m_Scene;
        string sceneName;

        void Start()
        {
            m_Scene = SceneManager.GetActiveScene();
            sceneName = m_Scene.name;
        }

        // if we are in the HQ outside and on the ladder, lower the transparency behind the cliff
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (sceneName == "06a HQ")
            {
                if (other.tag == "Player")
                {
                    other.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, .2f);
                }
            }
        }

        // return transparency to normal
        private void OnTriggerExit2D(Collider2D other)
        {
            if (sceneName == "06a HQ")
            {
                if (other.tag == "Player")
                {
                    other.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }
    }
}