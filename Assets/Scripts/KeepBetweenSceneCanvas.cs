﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace BreathingGames.Gameplay
{
    public class KeepBetweenSceneCanvas : MonoBehaviour
    {
        private static bool canvasUI = false;

        Scene m_Scene;
        string sceneName;

        private void Start()
        {
            m_Scene = SceneManager.GetActiveScene();
            sceneName = m_Scene.name;
        }

        void Awake()
        {
            if (!canvasUI && sceneName != "10 Lung Savior")
            {
                DontDestroyOnLoad(this.gameObject);
                canvasUI = true;
            }
        }
    }
}