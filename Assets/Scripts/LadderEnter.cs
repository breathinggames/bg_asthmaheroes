﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class LadderEnter : MonoBehaviour
    {

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = false;
            }
        }
    }
}