﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace BreathingGames.Character
{
    public class PlayerScenePosCorrection : MonoBehaviour
    {
        Scene m_Scene;
        string sceneName;

        void Start()
        {
            m_Scene = SceneManager.GetActiveScene();
            sceneName = m_Scene.name;
        }

        void Update()
        {
            ChangeParent();
        }

        public void ChangeParent()
        {
            this.transform.SetParent(GameObject.Find("Scene").transform);
        }
    }
}