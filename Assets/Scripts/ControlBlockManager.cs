﻿using UnityEngine;
using BreathingGames.UISystem;
using BreathingGames.Character;

namespace BreathingGames.Gameplay
{
    public class ControlBlockManager : MonoBehaviour
    {
        public void BlockedEvent()
        {
            GameObject.Find("Cell_Urgence").GetComponent<Cell>().enabled = false;

            if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
            {
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().ResetValue();
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().enabled = false;
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").GetComponent<Animator>().SetFloat("velocityX", 0);
            }

            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = false;
            }
        }

        public void UnblockedEvent()
        {
            GameObject.Find("Cell_Urgence").GetComponent<Cell>().enabled = true;

            if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
            {
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().ReInitValue();
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").GetComponent<PlayerPlatformerController>().enabled = true;
            }

            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = true;
            }
        }

        public void LockUI()
        {
            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = false;
            }
        }

        public void UnlockUI()
        {
            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = true;
            }
        }

        public void ActivePills()
        {
            if (GameObject.Find("Pills").transform.Find("Pills_shield"))
            {
                GameObject.Find("Pills").transform.Find("Pills_shield").gameObject.SetActive(true);
            }
        }

        public void ActiveMask()
        {
            if (GameObject.Find("Green Mask").transform.Find("Mask"))
            {
                GameObject.Find("Green Mask").transform.Find("Mask").gameObject.SetActive(true);
            }
        }
    }
}