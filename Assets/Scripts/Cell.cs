﻿using UnityEngine;

namespace BreathingGames.Gameplay
{
    public class Cell : MonoBehaviour
    {
        public GameObject cell;

        void Start()
        {
            cell.SetActive(false);
        }

        void Update()
        {
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.C))
            {
                cell.SetActive(true);
            }
        }
    }
}