﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class LadderStay : MonoBehaviour
    {

        public float climbSpeed = 2;

        void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "Player" && (ControlFreak2.CF2Input.GetKey(KeyCode.DownArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.S)) || (ControlFreak2.CF2Input.GetKey(KeyCode.UpArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.W)))
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = true;
            }
        }

    }
}