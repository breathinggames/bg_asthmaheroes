﻿using UnityEngine;

namespace BreathingGames.Gameplay
{
    public class KeepBetweenScene : MonoBehaviour
    {
        private static bool gameManager = false;

        void Awake()
        {
            if (!gameManager)
            {
                DontDestroyOnLoad(this.gameObject);
                gameManager = true;
            }
        }
    }
}