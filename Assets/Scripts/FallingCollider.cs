﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class FallingCollider : MonoBehaviour
    {

        public int healthDommage;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                GameObject.Find("Canvas").GetComponent<PlayerUI>().health.CurrentVal -= healthDommage;
            }
        }
    }
}