﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class PlayerReactivator : MonoBehaviour
    {

        void Update()
        {
            GameObject.Find("Scene").transform.Find("ColliderGround").gameObject.SetActive(true);

            if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
            {
                GameObject.Find("Scene").transform.Find("Player_girl(Clone)").gameObject.SetActive(true);
            }
        }
    }
}