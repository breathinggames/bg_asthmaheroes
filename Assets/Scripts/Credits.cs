﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        if (Screen.width <= 600)
        {
            transform.position = new Vector2(0, -22);
        }
    }

    void Update()
    {

        
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }
}
