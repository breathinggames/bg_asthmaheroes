﻿using UnityEngine;

public class TranslatePlayerToNewPos : MonoBehaviour {

    public GameObject targetPos;
    public GameObject reloadPos;

    public void MovePlayerToNewPos()
    {
        Transform player;

        if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
        {
            player = GameObject.Find("Scene").transform.Find("Player_girl(Clone)");
            player.transform.position = targetPos.transform.position;
        }     
    }

    public void Reload()
    {
        Transform player;

        if (GameObject.Find("Scene").transform.Find("Player_girl(Clone)"))
        {
            player = GameObject.Find("Scene").transform.Find("Player_girl(Clone)");
            player.transform.position = reloadPos.transform.position;
        }
    }
}
