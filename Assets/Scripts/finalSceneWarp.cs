﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

namespace BreathingGames.Gameplay
{
    public class FinalSceneWarp : MonoBehaviour
    {
        public Flowchart flowchartRef;

        public void Warp()
        {
            flowchartRef.ExecuteBlock("LoadScene");

            // SceneManager.LoadScene("12 End game");
        }
    }
}