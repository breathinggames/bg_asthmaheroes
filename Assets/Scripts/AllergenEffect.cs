﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class AllergenEffect : MonoBehaviour
    {

        // when entering an allergen field
        private void OnTriggerEnter2D(Collider2D other)
        {
            GameObject.Find("Canvas").GetComponent<PlayerUI>().isAllergenRegenActive = true;
        }

        // while being inside an allergen field
        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                if (!GameObject.Find("Canvas").GetComponent<PlayerUI>().isMaskOn)
                {
                    GameObject.Find("Canvas").GetComponent<PlayerUI>().allergen.CurrentVal -= 0.2f;
                }
                else
                {
                    GameObject.Find("Canvas").GetComponent<PlayerUI>().allergen.CurrentVal -= 0.1f;
                }                
            }
        }

        // when exiting an allergen field
        private void OnTriggerExit2D(Collider2D other)
        {
            GameObject.Find("Canvas").GetComponent<PlayerUI>().isAllergenRegenActive = false;
        }
    }
}