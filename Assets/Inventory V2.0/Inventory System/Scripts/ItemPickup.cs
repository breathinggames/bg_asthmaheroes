﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public class ItemPickup : MonoBehaviour
    {
        [SerializeField] private Item item;
        [SerializeField] private Inventory inventory;
        [SerializeField] private KeyCode itemPickupKeycode = KeyCode.Mouse0;

        private bool isInRange;

        private void OnValidate()
        {
            if (inventory == null)
            {
                inventory = FindObjectOfType<Inventory>();
            }
        }

        void Update()
        {
            if ( /*isInRange && */ ControlFreak2.CF2Input.GetKeyDown(itemPickupKeycode))  // uncomment if you need to validate the range
            {
                Item itemCopy = item.GetCopy();
                inventory.AddItem(itemCopy);
                itemCopy.Destroy();
            }
        }

        /// <summary>
        /// if player is in range (if required) for 3d       
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            CheckCollision(other.gameObject, true);
        }

        /// <summary>
        /// if player is no longer in range (if required) for 3d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit(Collider other)
        {
            CheckCollision(other.gameObject, false);
        }

        /// <summary>
        /// if player is in range (if required) for 2d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            CheckCollision(other.gameObject, true);
        }

        /// <summary>
        /// if player is no longer in range (if required) for 2d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit2D(Collider2D other)
        {
            CheckCollision(other.gameObject, false);
        }

        private void CheckCollision(GameObject gameObject, bool state)
        {
            if (gameObject.CompareTag("Player"))
            {
                isInRange = state;
            }
        }
    }
}