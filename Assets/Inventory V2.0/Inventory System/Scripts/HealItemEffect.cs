﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Breathing Games/Inventory System/Usable Item Effect/Heal Item")]
    public class HealItemEffect : UsableItemEffect
    {
        public int regenAmount;

        /// <summary>
        /// Execute the effect of the item on the stats
        /// </summary>
        /// <param name="parentItem"></param>
        /// <param name="characterStats"></param>
        public override void ExecuteEffect(UsableItem parentItem, InventorySystemManager characterStats)
        {
            characterStats.healthAmount += regenAmount;
        }

        /// <summary>
        /// Give a description of the effect of the current item
        /// </summary>
        /// <returns></returns>
        public override string GetDescription()
        {
            return "Heals for " + regenAmount + " health";
        }
    }
}