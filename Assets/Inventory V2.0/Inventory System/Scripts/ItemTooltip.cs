﻿using UnityEngine;
using UnityEngine.UI;

namespace BreathingGames.InventorySystem
{
    public class ItemTooltip : MonoBehaviour {

        [SerializeField] private Text itemNameText;
        [SerializeField] private Text itemTypeText;
        [SerializeField] private Text itemDescriptionText;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Show the tooltip about an item
        /// </summary>
        /// <param name="item"></param>
        public void ShowTooltip(Item item)
        {
            itemNameText.text = item.itemName;
            itemTypeText.text = item.GetItemType();
            itemDescriptionText.text = item.GetDescription();
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide the tooltip about an item
        /// </summary>
        public void HideTooltip()
        {
            gameObject.SetActive(false);
        }
    }
}