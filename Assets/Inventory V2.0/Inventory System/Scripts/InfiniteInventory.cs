﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public class InfiniteInventory : Inventory
    {
        [SerializeField] private GameObject itemSlotPrefab;

       [SerializeField] private int maxSlots;
        public int MaxSlots
        {
            get
            {
                return maxSlots;
            }

            set
            {
                SetMaxSlots(value);
            }
        }

        protected override void Awake()
        {
            // we need to have the item slots created before assigning the events from the base class
            SetMaxSlots(maxSlots);
            base.Awake();
        }

/*
        /// <summary>
        /// Can always add more item in the infinite inventory
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public override bool CanAddItem(Item item, int amount = 1)
        {
            return true;
        }

        /// <summary>
        /// add a slot while you can no longer add item so that you can always add more
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool AddItem(Item item)
        {
            while (!base.AddItem(item))
            {
                MaxSlots += 1;
            }
            return base.AddItem(item);
        }
*/

        /// <summary>
        /// set the value of the max item slots in the infinite inventory
        /// </summary>
        /// <param name="value"></param>
        private void SetMaxSlots(int value)
        {
            if (value <= 0)
            {
                maxSlots = 1;
            }
            else
            {
                maxSlots = value;
            }

            // if we have less or more item slots than current items, add or remove slots.
            if (maxSlots < itemSlots.Count)
            {
                // we need to destroy the parent game object. We start at maxSlot and we take out all slots after from the itemSlot
                for (int i = maxSlots; i < itemSlots.Count; i++)
                {
                    Destroy(itemSlots[i].transform.parent.gameObject);
                }

                // remove ref of item slots from the list. maxslot is a smaller value than the item slot so we substract it
                int diff =  itemSlots.Count - maxSlots;
                itemSlots.RemoveRange(maxSlots, diff);
            }
            else if (maxSlots > itemSlots.Count)
            {
                int diff = maxSlots - itemSlots.Count;

                for (int i = 0; i < diff; i++)
                {
                    GameObject itemSlotGameObj = Instantiate(itemSlotPrefab);
                    itemSlotGameObj.transform.SetParent(itemsParent, worldPositionStays: false);
                    itemSlots.Add(itemSlotGameObj.GetComponentInChildren<ItemSlot>());
                }
            }
        }
    }
}