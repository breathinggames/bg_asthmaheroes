﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BreathingGames.InventorySystem
{
    public class ItemSlot : BaseItemSlot, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
    {       
        public event Action<BaseItemSlot> OnBeginDragEvent;
        public event Action<BaseItemSlot> OnEndDragEvent;
        public event Action<BaseItemSlot> OnDragEvent;
        public event Action<BaseItemSlot> OnDropEvent;

        private Color dragColor = new Color(1, 1, 1, 0.5f);

        /// <summary>
        /// validate to see if we can add an item or a group of item and if the added items won't exceed the current limit for that stack
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public override bool CanAddStack(Item item, int amount = 1)
        {
            return base.CanAddStack(item, amount) && Amount + amount <= item.MaximumStack;
        }
        
        /// <summary>
        /// If can receive item in the slot == true. for normal item slot, it is always true.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool CanReceiveItem(Item item)
        {
            return true;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Item != null)
            {
                image.color = dragColor;
            }

            if (OnBeginDragEvent != null)
            {
                OnBeginDragEvent(this);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (Item != null)
            {
                image.color = normalColor;
            }

            if (OnEndDragEvent != null)
            {
                OnEndDragEvent(this);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (OnDragEvent != null)
            {
                OnDragEvent(this);
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (OnDropEvent != null)
            {
                OnDropEvent(this);
            }
        }
    }
}