﻿namespace BreathingGames.InventorySystem
{
    public class EquipmentSlot : ItemSlot
    {
        public EquipmentType equipmentType;

        protected override void OnValidate()
        {
            // first call the OnValidate from the BaseItemSlot
            base.OnValidate();
            gameObject.name = equipmentType.ToString() + " Slot";
        }

        /// <summary>
        /// overriden from ItemSlot virtual method
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool CanReceiveItem(Item item)
        {
            if (item == null)
            {
                return true;
            }

            EquippableItem equippableItem = item as EquippableItem;
            return equippableItem != null && equippableItem.equipmentType == equipmentType;
        }
    }
}