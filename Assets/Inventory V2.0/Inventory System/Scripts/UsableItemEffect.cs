﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public abstract class UsableItemEffect : ScriptableObject
    {
        public abstract void ExecuteEffect(UsableItem parentItem, InventorySystemManager CharacterStats);
        public abstract string GetDescription();
    }
}