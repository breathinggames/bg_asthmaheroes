﻿using UnityEngine;
using BreathingGames.PlayerStats;

namespace BreathingGames.InventorySystem
{
    // use this enum to add, modify or remove equipment types
    public enum EquipmentType
    {
        Head,
        Body,
        Hands,
        Legs,
        BluePomp,
        Pills,
        OrangePomp,
        Relic,
    }
}

namespace BreathingGames.InventorySystem
{
    [CreateAssetMenu(fileName = "New Equipment", menuName = "Breathing Games/Inventory System/Equipment")]
    public class EquippableItem : Item
    {
        // these fields relates to those of the Character class where you have originally created your main stats
        public int healthBonus;
        public int staminaBonus;
        public int lungCapacityBonus;       
        public int allergenBonus;
        [Space]
        public int healthPercentBonus;
        public int staminaPercentBonus;
        public int lungCapacityPercentBonus;        
        public int allergenPercentBonus;
        [Space]
        public EquipmentType equipmentType;

        /// <summary>
        /// Create an instance or copy of an item
        /// </summary>
        /// <returns></returns>
        public override Item GetCopy()
        {
            return Instantiate(this);
        }

        /// <summary>
        /// destroy the item
        /// </summary>
        public override void Destroy()
        {
            Destroy(this);
        }

        /// <summary>
        /// Add the modifier to the proper stat once it has been equipped
        /// </summary>
        /// <param name="data"></param>
        public void Equip(InventorySystemManager data)
        {
            if (healthBonus != 0)
            {
                data.health.AddModifier(new StatModifier(healthBonus, StatModType.Flat, this));
            }
            if (staminaBonus != 0)
            {
                data.stamina.AddModifier(new StatModifier(staminaBonus, StatModType.Flat, this));
            }
            if (lungCapacityBonus != 0)
            {
                data.lungCapacity.AddModifier(new StatModifier(lungCapacityBonus, StatModType.Flat, this));
            }
            if (allergenBonus != 0)
            {
                data.allergen.AddModifier(new StatModifier(allergenBonus, StatModType.Flat, this));
            }

            if (healthPercentBonus != 0)
            {
                data.health.AddModifier(new StatModifier(healthPercentBonus, StatModType.PercentMult, this));
            }
            if (staminaPercentBonus != 0)
            {
                data.stamina.AddModifier(new StatModifier(staminaPercentBonus, StatModType.PercentMult, this));
            }
            if (lungCapacityPercentBonus != 0)
            {
                data.lungCapacity.AddModifier(new StatModifier(lungCapacityPercentBonus, StatModType.PercentMult, this));
            }
            if (allergenPercentBonus != 0)
            {
                data.allergen.AddModifier(new StatModifier(allergenPercentBonus, StatModType.PercentMult, this));
            }
        }

        /// <summary>
        /// Remove the modifier to the proper stat once it has been unequipped
        /// </summary>
        /// <param name="data"></param>
        public void Unequip(InventorySystemManager data)
        {
            data.health.RemoveAllModifiersFromSource(this);
            data.stamina.RemoveAllModifiersFromSource(this);
            data.lungCapacity.RemoveAllModifiersFromSource(this);
            data.allergen.RemoveAllModifiersFromSource(this);
        }

        /// <summary>
        /// get the type of item being used
        /// </summary>
        /// <returns></returns>
        public override string GetItemType()
        {
            return equipmentType.ToString();
        }

        /// <summary>
        /// Get description about stats bonus
        /// </summary>
        /// <returns></returns>
        public override string GetDescription()
        {
            sb.Length = 0;
            AddStat(healthBonus, "Health");
            AddStat(staminaBonus, "Stamina");
            AddStat(lungCapacityBonus, "Lung Capacity");
            AddStat(allergenBonus, "Allergen");

            AddStat(healthPercentBonus, "Health", isPercent: true);
            AddStat(staminaPercentBonus, "Stamina", isPercent: true);
            AddStat(lungCapacityPercentBonus, "Lung Capacity", isPercent: true);
            AddStat(allergenPercentBonus, "Allergen", isPercent: true);

            return sb.ToString();
        }

        private void AddStat(float value, string statName, bool isPercent = false)
        {
            if (value != 0)
            {
                if (sb.Length > 0)
                {
                    sb.AppendLine();
                }

                if (value > 0)
                {
                    sb.Append("+");
                }

                if (isPercent)
                {
                    sb.Append(value * 100);
                    sb.Append("% ");
                }
                else
                {
                    sb.Append(value);
                    sb.Append(" ");
                }
                sb.Append(statName);
            }
        }
    }
}