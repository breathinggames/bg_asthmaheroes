﻿using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public class Inventory : ItemContainer
    {
        [SerializeField] protected List<Item> startingItems;
        [SerializeField] protected Transform itemsParent;

        protected override void Awake()
        {          
            // call the parent class Start first to assign the listeners to their slots
            base.Awake();

            // call this here to make it work in build settings since OnValidate only works in the editor
            SetStartingItem();
        }

        //once the ref has been set, it will auto populate this Game object list of children in the inspector in editor view
        protected override void OnValidate()
        {
            // No need to call the base version of the method since we already have our own custom way to get the items from the parent item
            if (itemsParent != null)
            {
                itemsParent.GetComponentsInChildren(includeInactive: true, result: itemSlots);
            }

            //update UI even if not in play mode
            if (!Application.isPlaying)
            {
                SetStartingItem();
            }
        }

        /// <summary>
        /// Set the items that you start with in your inventory
        /// </summary>
        private void SetStartingItem()
        {
            Clear();
            foreach (Item item in startingItems)
            {
                AddItem(item.GetCopy());
            }
        }       
    }
}