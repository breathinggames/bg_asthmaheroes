﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BreathingGames.InventorySystem
{
    public class BaseItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] protected Image image;
        [SerializeField] protected Text amountText;

        protected bool isPointerOver;

        // these are c# event, a delegate that check for an action of a certain kind - generic
        public event Action<BaseItemSlot> OnPointerEnterEvent;
        public event Action<BaseItemSlot> OnPointerExitEvent;
        public event Action<BaseItemSlot> OnRightClickEvent;

        protected Color normalColor = Color.white;
        protected Color disabledColor = new Color(1, 1, 1, 0);

        protected Item item;
        public Item Item
        {
            get
            {
                return item;
            }

            set
            {
                item = value;
                if (item == null && Amount != 0) Amount = 0;

                if (item == null)
                {
                    image.color = disabledColor;
                }
                else
                {
                    image.sprite = item.icon;
                    image.color = normalColor;
                }

                if (isPointerOver)
                {
                    OnPointerExit(null);
                    OnPointerEnter(null);
                }
            }
        }

        private int amount;
        public int Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;

                if (amount < 0)
                {
                    amount = 0;
                }

                if (amount == 0 && Item != null)
                {
                    Item = null;
                }

                if (amountText != null)
                {
                    amountText.enabled = item != null && amount > 1;
                    if (amountText.enabled)
                    {
                        amountText.text = amount.ToString();
                    }
                }
            }
        }

        //works only in the inspector and will force the placement of ref from the scene automatically
        protected virtual void OnValidate()
        {
            if (image == null)
            {
                image = GetComponent<Image>();
            }

            if (amountText == null)
            {
                amountText = GetComponentInChildren<Text>();
            }

            // refreh the UI of the item slot by reassigning the value of the item and amount
            Item = item;
            Amount = amount;
        }

        /// <summary>
        /// validate to see if we can add an item or a group of item (stack)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public virtual bool CanAddStack(Item item, int amount = 1)
        {
            return Item != null && Item.ID == item.ID;
        }

        public virtual bool CanReceiveItem(Item item)
        {
            return false;
        }

        protected virtual void OnDisable()
        {
            if (isPointerOver)
            {
                OnPointerExit(null);
            }
        }

        /// <summary>
        /// if the button that was clicked is the right mouse button
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData != null && eventData.button == PointerEventData.InputButton.Right)
            {
                if (OnRightClickEvent != null)
                {
                    OnRightClickEvent(this);
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            isPointerOver = true;

            if (OnPointerEnterEvent != null)
            {
                OnPointerEnterEvent(this);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            isPointerOver = false;

            if (OnPointerExitEvent != null)
            {
                OnPointerExitEvent(this);
            }
        }
    }
}