﻿using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BreathingGames.InventorySystem {

    [CreateAssetMenu(fileName = "New Item", menuName = "Breathing Games/Inventory System/Item")]
    public class Item : ScriptableObject {

        [SerializeField] string id;
        public string ID
        {
            get
            {
                return id;
            }
        }

        public string itemName;
        public Sprite icon;
        [Range(1,9)]public int MaximumStack = 1;

        protected static readonly StringBuilder sb = new StringBuilder();

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            // link to the original item from the copy created. All objects are different in memory, but will all share the same id
            // unity create a unique id identifier for all of its assets. We assign that to the id for each copy of that original item
            string path = AssetDatabase.GetAssetPath(this);
            id = AssetDatabase.AssetPathToGUID(path);
        }
#endif

        /// <summary>
        /// return the ref to the object itself from the copy that was created at runtime
        /// </summary>
        /// <returns></returns>
        public virtual Item GetCopy()
        {
            return this;
        }

        /// <summary>
        /// destroy the item
        /// </summary>
        public virtual void Destroy()
        {
            
        }

        public virtual string GetItemType()
        {
            return "";
        }

        public virtual string GetDescription()
        {
            return "";
        }
    }
}