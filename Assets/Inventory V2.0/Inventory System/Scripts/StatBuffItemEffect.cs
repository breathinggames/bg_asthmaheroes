﻿using BreathingGames.PlayerStats;
using System.Collections;
using UnityEngine;

namespace BreathingGames.InventorySystem
{
    [CreateAssetMenu(fileName = "New Item", menuName = "InventorySystem/UsableItemEffect/Stat Buff")]
    public class StatBuffItemEffect : UsableItemEffect
    {
        public int StaminaBuff;
        public float duration;

        public override void ExecuteEffect(UsableItem parentItem, InventorySystemManager characterStats)
        {
            StatModifier statModifier = new StatModifier(StaminaBuff, StatModType.Flat, parentItem);
            characterStats.stamina.AddModifier(statModifier);
            characterStats.UpdateStatValues();
            characterStats.StartCoroutine(RemoveBuff(characterStats, statModifier, duration));           
        }

        public override string GetDescription()
        {
            return "Grant " + StaminaBuff + " Agility for " + duration + " seconds.";
        }

        /// <summary>
        /// Remove the modifier after a certain amount of time
        /// </summary>
        /// <param name="characterStats"></param>
        /// <param name="statModifier"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        private static IEnumerator RemoveBuff(InventorySystemManager characterStats, StatModifier statModifier, float duration)
        {
            yield return new WaitForSeconds(duration);
            characterStats.stamina.RemoveModifier(statModifier);
            characterStats.UpdateStatValues();
        }
    }
}