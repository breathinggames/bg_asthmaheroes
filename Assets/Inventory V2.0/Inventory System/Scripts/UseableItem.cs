﻿using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.InventorySystem
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Breathing Games/Inventory System/Usable Item")]
    public class UsableItem : Item
    {
        public bool isConsumable;
        public List<UsableItemEffect> effects;

        public virtual void Use(InventorySystemManager characterStats)
        {
            foreach (UsableItemEffect effect in effects)
            {
                effect.ExecuteEffect(this, characterStats);
            }
        }

        /// <summary>
        /// Get the type of item that is being used
        /// </summary>
        /// <returns></returns>
        public override string GetItemType()
        {
            return isConsumable ? "Consumable" : "Usable";
        }

        /// <summary>
        /// get the description of item being used
        /// </summary>
        /// <returns></returns>
        public override string GetDescription()
        {
            sb.Length = 0;

            foreach (UsableItemEffect effect in effects)
            {
                sb.AppendLine(effect.GetDescription());
            }

            return sb.ToString();
        }
    }
}