﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public class ItemStash : ItemContainer
    {
        [SerializeField] private Transform itemsParent;
        [SerializeField] private KeyCode itemStashKeycode = KeyCode.Mouse0;

        private bool isInRange;
        private bool itemStashState;
        private InventorySystemManager inventorySystemManager;

        // Will depend upon fungus and the basic rules of the game for accessing a shop or talking to someone and might include a range property.

        protected override void OnValidate()
        {
            if (itemsParent != null)
            {
                itemsParent.GetComponentsInChildren<ItemSlot>(includeInactive: true, result: itemSlots);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            itemsParent.gameObject.SetActive(false);
        }

        void Update()
        {
            //the code will probably change. When we click on a npc the stash window activates amd it closes when we press the close button
            if (/* isInRange && */ ControlFreak2.CF2Input.GetKeyDown(itemStashKeycode))
            {
                itemStashState = !itemStashState;
                itemsParent.gameObject.SetActive(itemStashState);

                //call the method that changes the on click event behavior
                if (itemStashState)
                {
                    inventorySystemManager.OpenItemContainer(this);
                }
                else
                {
                    inventorySystemManager.CloseItemContainer(this);
                }
            }
        }

        /// <summary>
        /// if player is in range (if required) for 3d       
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            CheckCollision(other.gameObject, true);
        }

        /// <summary>
        /// if player is no longer in range (if required) for 3d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit(Collider other)
        {
            CheckCollision(other.gameObject, false);
        }

        /// <summary>
        /// if player is in range (if required) for 2d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            CheckCollision(other.gameObject, true);
        }

        /// <summary>
        /// if player is no longer in range (if required) for 2d
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit2D(Collider2D other)
        {
            CheckCollision(other.gameObject, false);
        }

        private void CheckCollision(GameObject gameObject, bool state)
        {
            if (gameObject.CompareTag("Player"))
            {
                isInRange = state;

                // if range is use and we can move while it is open
                if (!isInRange && itemStashState)
                {
                    itemStashState = false;
                    itemsParent.gameObject.SetActive(false);
                    inventorySystemManager.CloseItemContainer(this);
                }

                // when in range, get the inventory manager script ref from the player
                if (isInRange)
                {
                    inventorySystemManager = gameObject.GetComponent<InventorySystemManager>();
                }
                else
                {
                    inventorySystemManager = null;
                }
            }
        }
    }
}