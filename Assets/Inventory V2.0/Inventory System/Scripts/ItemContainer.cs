﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public abstract class ItemContainer : MonoBehaviour, IItemContainer
    {
        public List<ItemSlot> itemSlots;

        public event Action<BaseItemSlot> OnPointerEnterEvent;
        public event Action<BaseItemSlot> OnPointerExitEvent;
        public event Action<BaseItemSlot> OnRightClickEvent;
        public event Action<BaseItemSlot> OnBeginDragEvent;
        public event Action<BaseItemSlot> OnEndDragEvent;
        public event Action<BaseItemSlot> OnDragEvent;
        public event Action<BaseItemSlot> OnDropEvent;

        protected virtual void OnValidate()
        {
            //get components return an array. To get the result for a list, we need to use the result parameter
            GetComponentsInChildren(includeInactive: true, result: itemSlots);
        }

        protected virtual void Awake()
        {
            for (int i = 0; i < itemSlots.Count; i++)
            {
                // listener for the events specified in the itemSlots
                itemSlots[i].OnPointerEnterEvent += slot => EventHelper(slot, OnPointerEnterEvent);
                itemSlots[i].OnPointerExitEvent += slot => EventHelper(slot, OnPointerExitEvent);
                itemSlots[i].OnRightClickEvent += slot => EventHelper(slot, OnRightClickEvent);
                itemSlots[i].OnBeginDragEvent += slot => EventHelper(slot, OnBeginDragEvent);
                itemSlots[i].OnEndDragEvent += slot => EventHelper(slot, OnEndDragEvent);
                itemSlots[i].OnDragEvent += slot => EventHelper(slot, OnDragEvent);
                itemSlots[i].OnDropEvent += slot => EventHelper(slot, OnDropEvent);
            }
        }

        private void EventHelper(BaseItemSlot itemSlot, Action<BaseItemSlot> action)
        {
            if (action != null)
                action(itemSlot);
        }

        /// <summary>
        /// Check If we have enough free spaces for a specific item or stack of items 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public virtual bool CanAddItem(Item item, int amount = 1)
        {
            int freeSpace = 0;

            foreach (ItemSlot itemSlot in itemSlots)
            {
                // includes empty slots and slots occupied by an item with a value < than its maximumStack
                if (itemSlot.Item == null || itemSlot.Item.ID == item.ID)
                {
                    freeSpace += item.MaximumStack - itemSlot.Amount;
                }
            }

            return freeSpace >= amount;
        }

        /// <summary>
        /// Add an item to the inventory
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual bool AddItem(Item item)
        {
            for (int i = 0; i < itemSlots.Count; i++)
            {
                if (itemSlots[i].CanAddStack(item))
                {
                    itemSlots[i].Item = item;
                    itemSlots[i].Amount++;
                    return true;
                }
            }

            for (int i = 0; i < itemSlots.Count; i++)
            {
                if (itemSlots[i].Item == null)
                {
                    itemSlots[i].Item = item;
                    itemSlots[i].Amount++;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Remove an item from the inventory
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual bool RemoveItem(Item item)
        {
            for (int i = 0; i < itemSlots.Count; i++)
            {
                if (itemSlots[i].Item == item)
                {
                    itemSlots[i].Amount--;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Remove an item from the inventory by looking for its id
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public virtual Item RemoveItem(string itemID)
        {
            for (int i = 0; i < itemSlots.Count; i++)
            {
                Item item = itemSlots[i].Item;
                if (item != null && item.ID == itemID)
                {
                    itemSlots[i].Amount--;
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// return the number of items we have in the inventory
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public virtual int ItemCount(string itemID)
        {
            int number = 0;

            for (int i = 0; i < itemSlots.Count; i++)
            {
                Item item = itemSlots[i].Item;
                if (item != null && item.ID == itemID)
                {
                    number += itemSlots[i].Amount;
                }
            }
            return number;
        }

        public void Clear()
        {
            for (int i = 0; i < itemSlots.Count; i++)
            {
                if (itemSlots[i].Item != null && Application.isPlaying)
                {
                    itemSlots[i].Item.Destroy();
                }
                itemSlots[i].Item = null;
                itemSlots[i].Amount = 0;
            }
        }
    }
}