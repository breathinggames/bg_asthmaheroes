﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BreathingGames.InventorySystem;

namespace BreathingGames.CraftingSystem
{
    public class CraftingWindow : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private CraftingRecipeUI recipeUIPrefab;
        [SerializeField] private RectTransform recipeUIParent;
        [SerializeField] private List<CraftingRecipeUI> craftingRecipeUIs;

        [Header("Public Variables")]
        public ItemContainer itemContainer;
        public List<CraftingRecipe> craftingRecipes;

        public event Action<BaseItemSlot> OnPointerEnterEvent;
        public event Action<BaseItemSlot> OnPointerExitEvent;

        private void OnValidate()
        {
            Init();
        }

        void Start()
        {
            Init();

            foreach (CraftingRecipeUI craftingRecipeUI in craftingRecipeUIs)
            {
                craftingRecipeUI.OnPointerEnterEvent += slot => OnPointerEnterEvent(slot);
                craftingRecipeUI.OnPointerExitEvent += slot => OnPointerExitEvent(slot);
            }
        }

        /// <summary>
        /// Get every child, even the inactive ones from the recipeUI parent game object
        /// </summary>
        private void Init()
        {
            recipeUIParent.GetComponentsInChildren<CraftingRecipeUI>(includeInactive: true, result: craftingRecipeUIs);
            UpdateCraftingRecipes();
        }

        /// <summary>
        /// Add the proper amount of recipe object in the list
        /// </summary>
        public void UpdateCraftingRecipes()
        {
            for (int i = 0; i < craftingRecipes.Count; i++)
            {
                if (craftingRecipeUIs.Count == i)
                {
                    craftingRecipeUIs.Add(Instantiate(recipeUIPrefab, recipeUIParent, false));
                }
                else if (craftingRecipeUIs[i] == null)
                {
                    craftingRecipeUIs[i] = Instantiate(recipeUIPrefab, recipeUIParent, false);
                }

                craftingRecipeUIs[i].itemContainer = itemContainer;
                craftingRecipeUIs[i].CraftingRecipe = craftingRecipes[i];
            }

            for (int i = craftingRecipes.Count; i < craftingRecipeUIs.Count; i++)
            {
                craftingRecipeUIs[i].CraftingRecipe = null;
            }
        }
    }
}