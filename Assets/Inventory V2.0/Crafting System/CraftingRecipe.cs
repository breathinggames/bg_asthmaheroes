﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BreathingGames.InventorySystem;

namespace BreathingGames.CraftingSystem
{
    [Serializable]
    public struct ItemAmount
    {
        public InventorySystem.Item item;
        [Range(1, 9)] public int amount;
    }
}

namespace BreathingGames.CraftingSystem
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Breathing Games/Crafting System/Recipe")]
    public class CraftingRecipe : ScriptableObject {
        public List<ItemAmount> materials;
        public List<ItemAmount> results;

        /// <summary>
        /// Determine if we can craft the item or not
        /// </summary>
        /// <param name="itemContainer"></param>
        /// <returns></returns>
        public bool CanCraft(IItemContainer itemContainer)
        {
            return HasMaterials(itemContainer) && HasSpace(itemContainer);
        }

        /// <summary>
        /// If you have all required materials, craft the object
        /// </summary>
        /// <param name="itemContainer"></param>
        /// <returns></returns>
        private bool HasMaterials(IItemContainer itemContainer)
        {
            foreach (ItemAmount itemAmount in materials)
            {
                if (itemContainer.ItemCount(itemAmount.item.ID) < itemAmount.amount)
                {
                    Debug.LogWarning("You don't have the required materials.");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// If the inventory is not full, craft the item
        /// </summary>
        /// <param name="itemContainer"></param>
        /// <returns></returns>
        private bool HasSpace(IItemContainer itemContainer)
        {
            foreach (ItemAmount itemAmount in results)
            {
                if (!itemContainer.CanAddItem(itemAmount.item, itemAmount.amount))
                {
                    Debug.LogWarning("Your inventory is full.");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// craft the item and add it to the inventory while deleting the materials used to craft it from the inventory
        /// </summary>
        /// <param name="itemContainer"></param>
        public void Craft(IItemContainer itemContainer)
        {
            //TODO - fade item sprites and result sprite if we can't craft it and fully set the alpha if we can
            if (CanCraft(itemContainer))
            {
                RemoveMaterials(itemContainer);
                AddResults(itemContainer);
            }
        }

        /// <summary>
        /// remove materials from the inventory
        /// </summary>
        /// <param name="itemContainer"></param>
        private void RemoveMaterials(IItemContainer itemContainer)
        {
            foreach (ItemAmount itemAmount in materials)
            {
                for (int i = 0; i < itemAmount.amount; i++)
                {
                    InventorySystem.Item oldItem = itemContainer.RemoveItem(itemAmount.item.ID);
                    oldItem.Destroy();
                }
            }
        }

        /// <summary>
        /// add the crafted item to the inventory
        /// </summary>
        /// <param name="itemContainer"></param>
        private void AddResults(IItemContainer itemContainer)
        {
            foreach (ItemAmount itemAmount in results)
            {
                for (int i = 0; i < itemAmount.amount; i++)
                {
                    itemContainer.AddItem(itemAmount.item.GetCopy());
                }
            }
        }
    }
}