﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BreathingGames.InventorySystem;

namespace BreathingGames.CraftingSystem
{
    public class CraftingRecipeUI : MonoBehaviour
    {
        [Header("Reference")]
        [SerializeField] private RectTransform arrowParent;
        [SerializeField] private BaseItemSlot[] itemSlots;

        [Header("Public Variables")]
        public ItemContainer itemContainer;

        private CraftingRecipe craftingRecipe;
        public CraftingRecipe CraftingRecipe
        {
            get
            {
                return craftingRecipe;
            }

            set
            {
                SetCraftingRecipe(value);
            }
        }

        public event Action<BaseItemSlot> OnPointerEnterEvent;
        public event Action<BaseItemSlot> OnPointerExitEvent;

        private void OnValidate()
        {
            itemSlots = GetComponentsInChildren<BaseItemSlot>(includeInactive: true);
        }

        private void Start()
        {
            foreach (BaseItemSlot itemSlot in itemSlots)
            {
                itemSlot.OnPointerEnterEvent += slot => OnPointerEnterEvent(slot);
                itemSlot.OnPointerExitEvent += slot => OnPointerExitEvent(slot);
            }
        }

        /// <summary>
        /// Craft the item when pressing the craft button
        /// </summary>
        public void OnCraftButtonClick()
        {
            if (craftingRecipe != null && itemContainer != null)
            {
                craftingRecipe.Craft(itemContainer);
            }
        }

        /// <summary>
        /// Setup crafting recipe in the UI using its materials and its result data
        /// </summary>
        /// <param name="newCraftingRecipe"></param>
        private void SetCraftingRecipe(CraftingRecipe newCraftingRecipe)
        {
            craftingRecipe = newCraftingRecipe;

            if (craftingRecipe != null)
            {
                int slotIndex = 0;
                slotIndex = SetSlots(craftingRecipe.materials, slotIndex);
                arrowParent.SetSiblingIndex(slotIndex);
                slotIndex = SetSlots(craftingRecipe.results, slotIndex);

                for (int i = slotIndex; i < itemSlots.Length; i++)
                {
                    itemSlots[i].transform.parent.gameObject.SetActive(false);
                }

                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Setup the item and the amount of that item in the slot
        /// </summary>
        /// <param name="itemAmountList"></param>
        /// <param name="slotIndex"></param>
        /// <returns></returns>
        private int SetSlots(IList<ItemAmount> itemAmountList, int slotIndex)
        {
            for (int i = 0; i < itemAmountList.Count; i++, slotIndex++)
            {
                ItemAmount itemAmount = itemAmountList[i];
                BaseItemSlot itemSlot = itemSlots[slotIndex];

                itemSlot.Item = itemAmount.item;
                itemSlot.Amount = itemAmount.amount;
                itemSlot.transform.parent.gameObject.SetActive(true);
            }
            return slotIndex;
        }
    }
}