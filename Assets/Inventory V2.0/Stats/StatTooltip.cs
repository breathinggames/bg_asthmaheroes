﻿using BreathingGames.InventorySystem;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BreathingGames.PlayerStats
{
    public class StatTooltip : MonoBehaviour
    {
        [SerializeField] private Text statNameText;
        [SerializeField] private Text statModifiersLabelText;
        [SerializeField] private Text statModifiersText;

        // Using stringbuilder to avoid string concatenation
        private readonly StringBuilder sb = new StringBuilder();

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void ShowTooltip(CharacterStatsManager stat, string statName)
        {
            statNameText.text = GetStatTopText(stat, statName);
            statModifiersText.text = GetStatModifiersText(stat);
            gameObject.SetActive(true);
        }

        public void HideTooltip()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// create the text on the stat tooltip about the stat name, the final value, the base value itself and the value added and removed from based value
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="statName"></param>
        /// <returns></returns>
        private string GetStatTopText(CharacterStatsManager stat, string statName)
        {
            sb.Length = 0;
            sb.Append(statName);
            sb.Append(" ");
            sb.Append(stat.Value);

            if (stat.Value != stat.baseValue)
            {
                sb.Append(" (");
                sb.Append(stat.baseValue);

                if (stat.Value > stat.baseValue)
                {
                    sb.Append("+");
                }

                sb.Append(System.Math.Round(stat.Value - stat.baseValue, 0));
                sb.Append(")");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        private string GetStatModifiersText(CharacterStatsManager stat)
        {
            sb.Length = 0;

            foreach (StatModifier mod in stat.statModifiersReadOnly)
            {
                if (sb.Length > 0)
                {
                    // create a new line if there is already a mod on that one
                    sb.AppendLine();
                }

                if (mod.value > 0)
                {
                    sb.Append("+");
                }

                if (mod.modType == StatModType.Flat)
                {
                    sb.Append(mod.value);
                }
                else
                {
                    sb.Append(mod.value * 100);
                    sb.Append("%");
                }

                InventorySystem.Item item = mod.source as InventorySystem.Item;

                if (item != null)
                {
                    sb.Append(" ");
                    sb.Append(item.itemName);
                }
                else
                {
                    Debug.LogError("Modifier is not an Item");
                }
            }

            return sb.ToString();
        }
    }
}