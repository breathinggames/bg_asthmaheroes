﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BreathingGames.PlayerStats
{
    [Serializable]
    public class CharacterStatsManager
    {
        public float baseValue;

        // serves to indicate if we have to recalculate the value or not. Calculate value only if isDirty = true. 
        // set to true everytime a mod is added or deleted
        protected bool isDirty = true;        
        protected float lastBaseValue;

        // hold the most recent calculation that was performed by CalculateFinalValue()
        protected float modValue;
        public virtual float Value
        {
            get
            {
                if (isDirty || lastBaseValue != baseValue)
                {
                    lastBaseValue = baseValue;
                    modValue = CalculateFinalValue();
                    isDirty = false;
                }

                return modValue;
            }
        }

        // list of stat modifier that will hold all modifier applied to a stat
        protected readonly List<StatModifier> statModifiers;

        // this collection has a ref to the statModifiers collection but will prevent changes to it. 
        // To change this list, we must change statModifiers and this list will relfect the changes
        public readonly ReadOnlyCollection<StatModifier> statModifiersReadOnly;

        /// <summary>
        /// Default constructor with no param to init both collections to prevent null ref exception
        /// </summary>
        public CharacterStatsManager()
        {
            statModifiers = new List<StatModifier>();
            statModifiersReadOnly = statModifiers.AsReadOnly();
        }

        /// <summary>
        /// a constructor with a baseValue settings
        /// </summary>
        /// <param name="baseValue"></param>
        public CharacterStatsManager(float baseValue) : this()
        {
            this.baseValue = baseValue;
        }

        /// <summary>
        /// Add a modifier to the list of modifier
        /// </summary>
        /// <param name="mod"></param>
        public virtual void AddModifier(StatModifier mod)
        {
            isDirty = true;
            statModifiers.Add(mod);
            statModifiers.Sort(CompareModifierOrder);
        }

        /// <summary>
        /// Remove a modifier from the list of modifier
        /// </summary>
        /// <param name="mod"></param>
        /// <returns></returns>
        public virtual bool RemoveModifier(StatModifier mod)
        {
            // set isDirty to true only if we are actually removing something. we don't do it if there is nothing to remove
            if (statModifiers.Remove(mod))
            {
                isDirty = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Use the source var to remove all modifiers from a specific source like an item that you unequipped
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public virtual bool RemoveAllModifiersFromSource(object source)
        {
            bool didRemove = false;

            // traverse the list in reverse to delete elements from the list to prevent the other objects in the list to change index -1 at every step
            for (int i = statModifiers.Count - 1; i >= 0; i--)
            {
                if (statModifiers[i].source == source)
                {
                    isDirty = true;
                    didRemove = true;
                    statModifiers.RemoveAt(i);
                }
            }

            return didRemove;
        }

        /// <summary>
        /// Comparison function that uses the order var from StatModifier to sort the order in which the modifier will be added to the stat
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        protected virtual int CompareModifierOrder(StatModifier a, StatModifier b)
        {
            // a comparison has 3 different possible outcomes ( a come before b; a come after b; and a and b have the same priority in the stack of modifiers)
            if (a.order < b.order)
            {
                return -1;
            }
            else if (a.order > b.order)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Calculate the value for a list of modifier types
        /// </summary>
        /// <returns></returns>
        protected virtual float CalculateFinalValue()
        {
            float finalValue = baseValue;
            float sumPercentAdd = 0;

            // we go through the list of stat modifiers and we add them together
            for (int i = 0; i < statModifiers.Count; i++)
            {

                StatModifier mod = statModifiers[i];

                // modify how we calculate modifiers depending on the type of the modifier
                if (mod.modType == StatModType.Flat)
                {
                    finalValue += mod.value;
                }
                else if (mod.modType == StatModType.PercentAdd)
                {
                    sumPercentAdd += mod.value;

                    // we iterate through the list and we add all modifiers of the same type until we reach a modifier of a different type or the end of the list
                    if (i + 1 >= statModifiers.Count || statModifiers[i + 1].modType != StatModType.PercentAdd)
                    {
                        finalValue *= 1 + sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                }
                else if (mod.modType == StatModType.PercentMult)
                {
                    finalValue *= 1 + mod.value;
                }
            }

            // to make it more precise, we can add (, 4) after finla value to prevent float calculation error
            return (float)Math.Round(finalValue);
        }
    }
}