﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BreathingGames.PlayerStats
{
    public class StatDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private CharacterStatsManager stat;
        public CharacterStatsManager Stat {
            get
            {
                return stat;
            }
            set
            {
                stat = value;
                UpdateStatValue();
            }
        }

        private string statName;
        public string Name {
            get
            {
                return statName;
            }
            set
            {
                statName = value;
                nameText.text = statName.ToLower();
            }
        }

        [SerializeField] private Text nameText;
        [SerializeField] private Text valueText;
        [SerializeField] private StatTooltip tooltip;

        private bool showingTooltip;

        private void OnValidate()
        {
            // when there is many text on every children to set, ref them in the order they appear in the hierarchy
            Text[] texts = GetComponentsInChildren<Text>();
            nameText = texts[0];
            valueText = texts[1];

            if (tooltip == null)
            {
                tooltip = FindObjectOfType<StatTooltip>();
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            tooltip.ShowTooltip(Stat, Name);
            showingTooltip = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            tooltip.HideTooltip();
            showingTooltip = false;
        }

        public void UpdateStatValue()
        {
            valueText.text = stat.Value.ToString();
            if (showingTooltip)
            {
                tooltip.ShowTooltip(Stat, Name);
            }
        }
    }
}