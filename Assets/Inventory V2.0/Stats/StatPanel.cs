﻿using UnityEngine;

namespace BreathingGames.PlayerStats
{
    public class StatPanel : MonoBehaviour
    {
        [SerializeField] StatDisplay[] statDisplays;
        [SerializeField] string[] statNames;

        private CharacterStatsManager[] stats;

        private void OnValidate()
        {
            statDisplays = GetComponentsInChildren<StatDisplay>();
            UpdateStatNames();
        }

        /// <summary>
        /// array of character stats that we can assign directly to our stat var
        /// </summary>
        /// <param name="charStats"></param>
        public void SetStats(params CharacterStatsManager[] charStats)
        {
            stats = charStats;

            if (stats.Length > statDisplays.Length)
            {
                Debug.LogError("Not enough Stat Display");
                return;
            }

            for (int i = 0; i < statDisplays.Length; i++)
            {
                //activate every stat display in the array that has the same length than the number of actual stats. Ignore the rest
                statDisplays[i].gameObject.SetActive(i < stats.Length);

                if (i < stats.Length)
                {
                    statDisplays[i].Stat = stats[i];
                }
            }
        }

        /// <summary>
        /// update values in the UI
        /// </summary>
        public void UpdateStatValues()
        {
            for (int i = 0; i < stats.Length; i++)
            {
                statDisplays[i].UpdateStatValue();
            }
        }

        /// <summary>
        /// update names in the UI
        /// </summary>
        public void UpdateStatNames()
        {
            for (int i = 0; i < statNames.Length; i++)
            {
                statDisplays[i].Name = statNames[i];
            }
        }
    }
}