﻿namespace BreathingGames.PlayerStats
{
    /// <summary>
    /// Define the types of stat modifiers. The override value of 100, 200, 300 is to make sure that there are values 
    /// between each type in case we wish to add a type between those already present
    /// </summary>
    public enum StatModType
    {
        Flat = 100,         // add the value not as a %
        PercentAdd = 200,   // add the different values
        PercentMult = 300,  // multiply the different values
    }
}

namespace BreathingGames.PlayerStats
{
    public class StatModifier
    {
        public readonly float value;
        public readonly StatModType modType;
        public readonly int order;
        public readonly object source;  // object class can hold any types we want

        /// <summary>
        /// Constructor that defines the base value of the modifer, its type, the order in which it should be applied in the stack of modifiers.
        /// All variables has to be set in this use case.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="modType"></param>
        /// <param name="order"></param>
        /// <param name="source"></param>
        public StatModifier(float value, StatModType modType, int order, object source)
        {
            this.value = value;
            this.modType = modType;
            this.order = order;
            this.source = source;  // we can use the source to provide info into where each mofidier came from (which item for ex) and use the source to remove all mod from a specific item
        }

        /// <summary>
        /// In this constructor, the user only has to input the value and the type. The this part automatically call the previous contructor
        /// Value and type are required but the other 2 are optional
        /// </summary>
        /// <param name="value"></param>
        /// <param name="modType"></param>
        public StatModifier(float value, StatModType modType) : this(value, modType, (int)modType, null) { }

        /// <summary>
        /// In this constructor, the user only has to input the value, the type and the order. The this part automatically call the previous contructor
        /// Value, type  and order are required but the other is optional
        /// </summary>
        /// <param name="value"></param>
        /// <param name="modType"></param>
        /// <param name="order"></param>
        public StatModifier(float value, StatModType modType, int order) : this(value, modType, order, null) { }

        /// <summary>
        /// In this constructor, the user only has to input the value, the type and the source. The this part automatically call the previous contructor
        /// Value, type and source are required but the other is optional
        /// </summary>
        /// <param name="value"></param>
        /// <param name="modType"></param>
        /// <param name="source"></param>
        public StatModifier(float value, StatModType modType, object source) : this(value, modType, (int)modType, source) { }
    }
}