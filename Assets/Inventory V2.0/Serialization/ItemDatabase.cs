﻿using UnityEngine;
#if UNITY_EDITOR // work in editor only, not in build
using UnityEditor;
#endif

namespace BreathingGames.InventorySystem.Serialization
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Breathing Games/Inventory System/Serialization/Item database")]
    public class ItemDatabase : ScriptableObject
    {
        // database array
        [SerializeField] private Item[] items;

        /// <summary>
        /// get item from the database by its id
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item GetItemReference(string itemID)
        {
            foreach (Item item in items)
            {
                if (item.ID == itemID)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// return a copy of the item from the database through its id
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public Item GetItemCopy(string itemID)
        {
            Item item = GetItemReference(itemID);
            return item != null ? item.GetCopy() : null;
        }

#if UNITY_EDITOR  // will compile only when in the editor to prevent compilation error. Will not work in a build
        private void OnValidate()
        {
            LoadItems();
        }

        /// <summary>
        /// call every time something in the project changes
        /// </summary>
        private void OnEnable()
        {
            EditorApplication.projectChanged -= LoadItems;
            EditorApplication.projectChanged += LoadItems;
        }

        /// <summary>
        /// call every time something in the project changes
        /// </summary>
        private void OnDisable()
        {
            EditorApplication.projectChanged -= LoadItems;
        }

        /// <summary>
        /// provide the asset path to where our items are located in the project folder
        /// </summary>
        private void LoadItems()
        {
            items = FindAssetsByType<Item>("Assets/Inventory V2.0/Inventory System/Item Assets");
        }

        // Slightly modified version of this answer: http://answers.unity.com/answers/1216386/view.html
        // it finds all assets of a specified type
        public static T[] FindAssetsByType<T>(params string[] folders) where T : Object
        {
            // convert the type we supply into a string, we remove the unity engine namespace from the type name
            string type = typeof(T).ToString().Replace("UnityEngine.", "");

            // this will not work if it contains the unity engine namespace
            // get the guids of all assets of the specified types we are looking for
            string[] guids;
            if (folders == null || folders.Length == 0)
            {
                guids = AssetDatabase.FindAssets("t:" + type);
            }
            else
            {
                // if we pass an argument in the folders array, we filter our document using the array
                guids = AssetDatabase.FindAssets("t:" + type, folders);
            }

            // create a new array the same length of the number of id found
            T[] assets = new T[guids.Length];

            // we get the location of the aaset and then we load the asset at that path and we save it at the corresponding index of the asset array
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                assets[i] = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            }
            return assets;
        }
#endif
    }
}