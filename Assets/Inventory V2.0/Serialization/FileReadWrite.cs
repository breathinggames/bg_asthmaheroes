﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BreathingGames.InventorySystem.Serialization
{
    public static class FileReadWrite
    {
        /// <summary>
        /// This will write an object of any type to the filepath that you choose
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filepath"></param>
        /// <param name="objectToWrite"></param>
        public static void WriteToBinaryFile<T>(string filepath, T objectToWrite)
        {
            // we open the file or create it if it doesn't exist
            using (Stream stream = File.Open(filepath, FileMode.Create)) {
                
                // convert data into binary and write it to the file
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// we open the file, read the file and convert it to the type that we choose and return it
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static T ReadFromBinaryFile<T>(string filepath)
        {
            // auto close the file when the statement ends without having to call stream.close - this is what the using statement does in both methods here
            using (Stream stream = File.Open(filepath, FileMode.Open))
            {                
                var binaryFormatter = new BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }
    }
}