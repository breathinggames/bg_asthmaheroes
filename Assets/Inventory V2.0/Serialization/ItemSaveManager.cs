﻿using System.Collections.Generic;
using UnityEngine;

namespace BreathingGames.InventorySystem.Serialization
{
    public class ItemSaveManager : MonoBehaviour
    {
        private const string inventoryFileName = "Inventory";
        private const string EquipmentFileName = "Equipment";

        [SerializeField] private ItemDatabase itemDatabase;

        /// <summary>
        /// Load data into the inventory from the save data container
        /// </summary>
        /// <param name="inventorySystemManager"></param>
        public void LoadInventory(InventorySystemManager inventorySystemManager)
        {
            // if there is a saved data, load it
            ItemContainerSaveData savedSlots = ItemSaveIO.LoadItems(inventoryFileName);
            if (savedSlots == null)
            {
                return;
            }

            //remove items in the current inventory
            inventorySystemManager.inventory.Clear();

            // assign items in the save item slot to the inventory
            for (int i = 0; i < savedSlots.savedSlots.Length; i++)
            {
                ItemSlot itemSlot = inventorySystemManager.inventory.itemSlots[i];
                ItemSlotSaveData savedSlot = savedSlots.savedSlots[i];

                if (savedSlot == null)
                {
                    itemSlot.Item = null;
                    itemSlot.Amount = 0;
                }
                else
                {
                    itemSlot.Item = itemDatabase.GetItemCopy(savedSlot.itemID);
                    itemSlot.Amount = savedSlot.amount;
                }
            }
        }

        /// <summary>
        /// Load data into the equipment panel from the save data container
        /// </summary>
        /// <param name="inventorySystemManager"></param>
        public void LoadEquipment(InventorySystemManager inventorySystemManager)
        {
            // if there is a saved data, load it
            ItemContainerSaveData savedSlots = ItemSaveIO.LoadItems(EquipmentFileName);
            if (savedSlots == null)
            {
                return;
            }

            // loop trhough the saved slots and add them to the equipment panel
            foreach (ItemSlotSaveData savedSlot in savedSlots.savedSlots)
            {
                if (savedSlot == null)
                {
                    continue;
                }

                // we need to call the equip method to make sure that the stats on the items are passed as well
                Item item = itemDatabase.GetItemCopy(savedSlot.itemID);
                inventorySystemManager.inventory.AddItem(item);
                inventorySystemManager.Equip((EquippableItem)item);
            }
        }

        /// <summary>
        /// save items from the inventory
        /// </summary>
        /// <param name="inventorySystemManager"></param>
        public void SaveInventory(InventorySystemManager inventorySystemManager)
        {
            SaveItems(inventorySystemManager.inventory.itemSlots, inventoryFileName);
        }

        /// <summary>
        /// save items from the equipment panel
        /// </summary>
        /// <param name="inventorySystemManager"></param>
        public void SaveEquipment(InventorySystemManager inventorySystemManager)
        {
            SaveItems(inventorySystemManager.equipmentPanel.equipmentSlots, EquipmentFileName);
        }

        /// <summary>
        /// convert the inventory and equipment item slots into the item container save data format. the IList is implemented
        /// in both an array and a list and so, by passing it as an argument, we can use both arrays and list as required and will work the same way
        /// this makes the code more generic
        /// </summary>
        /// <param name="itemSlots"></param>
        /// <param name="fileName"></param>
        private void SaveItems(IList<ItemSlot> itemSlots, string fileName)
        {
            var saveData = new ItemContainerSaveData(itemSlots.Count);

            for (int i = 0; i < saveData.savedSlots.Length; i++)
            {
                ItemSlot itemSlot = itemSlots[i];

                if (itemSlot.Item == null)
                {
                    saveData.savedSlots[i] = null;
                }
                else
                {
                    saveData.savedSlots[i] = new ItemSlotSaveData(itemSlot.Item.ID, itemSlot.Amount);
                }
            }

            ItemSaveIO.SaveItems(saveData, fileName);
        }
    }
}