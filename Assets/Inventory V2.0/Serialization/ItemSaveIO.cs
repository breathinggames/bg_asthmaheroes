﻿using UnityEngine;

namespace BreathingGames.InventorySystem.Serialization
{
    public static class ItemSaveIO
    {
        private static readonly string baseSavePath;

        // a static constructor is run automatically the first time any static method or var is used in that class
        static ItemSaveIO()
        {
            // value is cache in a static constructor
            baseSavePath = Application.persistentDataPath;
        }

        /// <summary>
        /// Saving items to a file
        /// </summary>
        /// <param name="items"></param>
        /// <param name="fileName"></param>
        public static void SaveItems(ItemContainerSaveData items, string fileName)
        {
            // we create the full save path of the file
            FileReadWrite.WriteToBinaryFile(baseSavePath + "/" + fileName + ".dat", items);
        }

        /// <summary>
        /// load items from a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ItemContainerSaveData LoadItems(string fileName)
        {
            string filepath = baseSavePath + "/" + fileName + ".dat";

            if (System.IO.File.Exists(filepath))
            {
                return FileReadWrite.ReadFromBinaryFile<ItemContainerSaveData>(filepath);
            }
            return null;
        }
    }
}