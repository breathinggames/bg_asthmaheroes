﻿using System;

namespace BreathingGames.InventorySystem.Serialization
{
    [Serializable]
    public class ItemSlotSaveData
    {
        // represents the data we want to save for each item slots
        public string itemID;
        public int amount;

        // constructor
        public ItemSlotSaveData(string id, int amount)
        {
            itemID = id;
            this.amount = amount;
        }
    }

    [Serializable]
    public class ItemContainerSaveData
    {
        // represents the data we want to save for the entire inventory or equipment panel
        public ItemSlotSaveData[] savedSlots;

        // constructor
        public ItemContainerSaveData(int numItems)
        {
            savedSlots = new ItemSlotSaveData[numItems];
        }
    }    
}