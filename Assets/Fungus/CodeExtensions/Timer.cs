﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[EventHandlerInfo("Timers", "Simple Timer", "Executes the block after an amount of time has elapsed")]
public class Timer : EventHandler {

    public float duration;

	void Start () {
        Invoke("TimerExpired", duration);
	}

    private void TimerExpired()
    {
        ExecuteBlock();
    }
}
