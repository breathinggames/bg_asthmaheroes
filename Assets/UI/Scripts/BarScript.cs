﻿using UnityEngine;
using UnityEngine.UI;

namespace BreathingGames.UISystem
{
    public class BarScript : MonoBehaviour
    {
        private float fillAmount;

        [SerializeField] private float lerpSpeed;
        [SerializeField] private Image content;
        [SerializeField] private GameObject warning;
        [SerializeField] private Text valueText;

        // change the UI color while it decrease and if the option is checked in the inspector
        [SerializeField] private Color fullColor;
        [SerializeField] private Color lowColor;
        [SerializeField] private bool isLerpColor;

        public float MaxValue { get; set; }
        public float Value
        {
            set
            {
                // split index after a :
                string[] temp = valueText.text.Split(':');
                valueText.text = temp[0] + ": " + value;

                fillAmount = SliderValueMapper(value, 0, MaxValue, 0, 1);

                if (value <= 20f)
                {
                    warning.SetActive(true);
                }
                else if (value > 20f)
                {
                    warning.SetActive(false);
                }
            }
        }

        void Start()
        {
            warning.SetActive(false);

            if (isLerpColor)
            {
                content.color = fullColor;
            }
        }

        void Update()
        {
            HandleBar();
        }

        // when current value changes, we update the slider value
        private void HandleBar()
        {
            if (fillAmount != content.fillAmount)
            {
                content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
            }

            // if check will work otherwise, the default color will be used
            if (isLerpColor)
            {
                content.color = Color.Lerp(lowColor, fullColor, fillAmount);
            }
        }

        // we take the actual value, the min and max it can have with normal numbers (in) and convert it to the slider value (out) between 0 - 1
        private float SliderValueMapper(float value, float inMin, float inMax, float outMin, float outMax)
        {
            return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        }
    }
}