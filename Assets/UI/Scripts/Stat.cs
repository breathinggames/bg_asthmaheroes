﻿using System;
using UnityEngine;

namespace BreathingGames.UISystem
{
    [Serializable]
    public class Stat
    {
        [SerializeField] private BarScript bar;
        [SerializeField] private float maxVal;
        [SerializeField] private float currentVal;

        // the current value of the bar will be clamped to map the fillAmount
        public float CurrentVal
        {
            get
            {
                return currentVal;
            }

            set
            {
                this.currentVal = Mathf.Clamp(value, 0, MaxVal);
                bar.Value = Mathf.Round(currentVal);
            }
        }

        public float MaxVal
        {
            get
            {
                return maxVal;
            }

            set
            {
                this.maxVal = value;
                bar.MaxValue = maxVal;
            }
        }

        /// <summary>
        /// Init UI slider Value
        /// </summary>
        public void Initialize()
        {
            this.MaxVal = maxVal;
            this.CurrentVal = currentVal;
        }
    }
}