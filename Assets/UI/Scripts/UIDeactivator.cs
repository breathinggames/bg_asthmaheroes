﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class UIDeactivator : MonoBehaviour
    {
        void Update()
        {
            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = false;
            }
        }
    }
}