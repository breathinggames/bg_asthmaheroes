﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class UIActivator : MonoBehaviour
    {
        void Update()
        {
            if (GameObject.Find("Canvas").activeInHierarchy)
            {
                GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
                GameObject.Find("Canvas").GetComponent<PlayerUI>().enabled = true;
            }
        }
    }
}