﻿using UnityEngine;

namespace BreathingGames.UISystem
{
    public class CloseUIForSpaceShooter : MonoBehaviour
    {

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                if (GameObject.Find("Canvas").activeInHierarchy)
                {
                    GameObject.Find("Canvas").SetActive(false);
                }
            }
        }
    }
}