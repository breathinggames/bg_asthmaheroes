**ProjectAsthmaHeroes**

Étape 1 : Se connecter à GitLab

    Ouvrez votre navigateur Internet (comme Google Chrome, Safari ou Edge).
    Tapez l’adresse du site GitLab (par exemple, https://gitlab.com) dans la barre d’adresse, puis appuyez sur Entrée.
    Si demandé, connectez-vous avec votre adresse e-mail et votre mot de passe.
        Si vous n’avez pas de compte, demandez à quelqu’un de vous en créer un.

Étape 2 : Trouver le bon dossier

    Une fois connecté, vous verrez une liste de projets ou un tableau de bord.
    Cliquez sur le projet où se trouve le jeu (demandez de l’aide si vous n’êtes pas sûr du nom).
    Quand le projet s’ouvre, cherchez le dossier nommé “Builds” dans la liste.
        Cela peut être visible directement ou il faudra peut-être cliquer sur un lien pour l’ouvrir.

Étape 3 : Choisir la version pour votre appareil

    Une fois dans le dossier Builds, vous verrez trois sous-dossiers :
        PC : pour les ordinateurs Windows.
        Mac : pour les ordinateurs Apple.
        Android : pour les téléphones ou tablettes Android.
    Cliquez sur le dossier correspondant à votre appareil.
        Si vous ne savez pas quel dossier choisir, demandez à quelqu’un de vous aider.

Étape 4 : Télécharger le fichier

    Dans le dossier de votre plateforme (par exemple, PC ou Android), vous verrez des fichiers.
        Parfois, il s’agit d’un seul fichier compressé, avec l’extension .zip.
        Parfois, ce sont plusieurs fichiers ou un dossier complet.
    Cliquez sur le fichier ou le dossier que vous voulez télécharger.
    Une fois que le fichier est ouvert :
        Si vous voyez un bouton ou un lien Télécharger, cliquez dessus.
        Si ce n’est pas clair, regardez en haut à droite de l’écran et cherchez un bouton qui dit Télécharger comme ZIP. Cliquez dessus.

Étape 5 : Ouvrir ou installer le build

    Une fois le téléchargement terminé :
        Si c’est un fichier .zip, double-cliquez dessus pour ouvrir ou extraire son contenu (l’ordinateur fera cela automatiquement dans beaucoup de cas).
        Si c’est un fichier .apk (pour Android), transférez-le sur votre téléphone pour l’installer.
    Dans le dossier extrait :
        Sur PC : Trouvez le fichier avec une icône ressemblant à une fenêtre et un nom se terminant par .exe. Double-cliquez dessus pour lancer le jeu.
        Sur Mac : Trouvez un fichier se terminant par .app, puis ouvrez-le.
        Sur Android : Allez dans vos fichiers, trouvez l’.apk, et appuyez dessus pour l’installer (vous devrez peut-être autoriser les "sources inconnues").

Étape 6 : Vérifier que tout fonctionne

    Lancez le jeu et testez si tout fonctionne.
    Si vous voyez un problème ou si le fichier ne s’ouvre pas, assurez-vous que vous avez téléchargé le bon fichier pour votre appareil.

Conseils supplémentaires

    Si plusieurs fichiers sont dans le dossier, cherchez toujours la version la plus récente (souvent, elle a une date ou un numéro plus élevé, comme "v1.2").
    Gardez les versions précédentes quelque part, au cas où vous auriez besoin de revenir à une ancienne version.

Si vous avez besoin d’aide à n’importe quelle étape, demandez à un ami, un collègue ou un membre de la famille ! 