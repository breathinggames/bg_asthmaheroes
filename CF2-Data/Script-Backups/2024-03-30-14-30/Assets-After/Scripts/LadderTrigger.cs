﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class LadderTrigger : MonoBehaviour
    {

        public float climbSpeed = 2;

        void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "Player" && (ControlFreak2.CF2Input.GetKey(KeyCode.DownArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.S)))
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = true;
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = false;
            }
        }
    }
}