﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class LadderStay : MonoBehaviour
    {

        public float climbSpeed = 2;

        void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "Player" && (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) || (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)))
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = true;
            }
        }

    }
}