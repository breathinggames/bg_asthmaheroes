﻿using UnityEngine;

namespace BreathingGames.Character
{
    public class LadderTriggerGoingUp : MonoBehaviour
    {

        public float climbSpeed = 2;

        void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "Player" && (Input.GetKey(KeyCode.UpArrow) || (Input.GetKey(KeyCode.W))))
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = true;
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                other.GetComponent<PlayerPlatformerController>().isOnLadder = false;
                climbSpeed = 0;
            }
        }
    }
}