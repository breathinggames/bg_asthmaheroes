﻿using UnityEngine;

namespace BreathingGames.InventorySystem
{
    public class InventoryInput : MonoBehaviour
    {
        // used to configure from the inspector
        [SerializeField] private GameObject inventoryPanelGameObject;
        [SerializeField] private GameObject equipmentPanelGameObject;
        [SerializeField] private GameObject craftingPanelGameObject;
        [SerializeField] private KeyCode[] toggleInventoryKeys;
        [SerializeField] private KeyCode[] toggleEquipmentPanelKeys;
        [SerializeField] private KeyCode[] toggleCraftingPanelKeys;
        [SerializeField] private bool gameUseMouseInput;
        
        void Update()
        {
            ToggleInventorySystem();
            ToggleEquipmentSystem();
            ToggleCraftingSystem();
        }

        /// <summary>
        /// Toggle equipment panel on or off using key input
        /// </summary>
        private void ToggleEquipmentSystem()
        {
            for (int i = 0; i < toggleEquipmentPanelKeys.Length; i++)
            {
                if (Input.GetKeyDown(toggleEquipmentPanelKeys[i]))
                {
                    equipmentPanelGameObject.SetActive(!equipmentPanelGameObject.activeSelf);
                    DoGameUseMouseInput();
                    break;
                }
            }
        }

        /// <summary>
        /// Toggle crafting panel on or off using key input
        /// </summary>
        private void ToggleCraftingSystem()
        {
            for (int i = 0; i < toggleCraftingPanelKeys.Length; i++)
            {
                if (Input.GetKeyDown(toggleCraftingPanelKeys[i]))
                {
                    craftingPanelGameObject.SetActive(!craftingPanelGameObject.activeSelf);
                    DoGameUseMouseInput();
                    break;
                }
            }
        }

        /// <summary>
        ///  Toggle inventory panel on or off using key input
        /// </summary>
        private void ToggleInventorySystem()
        {
            for (int i = 0; i < toggleInventoryKeys.Length; i++)
            {
                if (Input.GetKeyDown(toggleInventoryKeys[i]))
                {
                    inventoryPanelGameObject.SetActive(!inventoryPanelGameObject.activeSelf);
                    DoGameUseMouseInput();
                    break;
                }
            }
        }

        /// <summary>
        /// Use this only if you need to show or hide cursor in your game
        /// </summary>
        private void DoGameUseMouseInput()
        {
            if (!gameUseMouseInput)
            {
                if (inventoryPanelGameObject.activeSelf)
                {
                    ShowCursor();
                }
                else
                {
                    HideCursor();
                }
            }
        }

        /// <summary>
        /// Show mouse cursor and allow movement of cursor
        /// </summary>
        public void ShowCursor()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        /// <summary>
        /// Hide mouse cursor and prevent movement of cursor
        /// </summary>
        public void HideCursor()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        /// <summary>
        /// Toggle equipment panel on or off using button from UI
        /// </summary>
        public void ToggleEquipmentPanel()
        {
            craftingPanelGameObject.SetActive(false);
            equipmentPanelGameObject.SetActive(!equipmentPanelGameObject.activeSelf);
        }

        /// <summary>
        /// Toggle crafting panel on or off using button from UI
        /// </summary>
        public void ToggleCraftingPanel()
        {
            equipmentPanelGameObject.SetActive(false);
            craftingPanelGameObject.SetActive(!craftingPanelGameObject.activeSelf);
        }
    }
}