﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    public class Ship : MonoBehaviour
    {

        public float speed;
        private Rigidbody2D rb;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            transform.position = new Vector2(Mathf.Clamp(transform.position.x, -5, 5), Mathf.Clamp(transform.position.y, -3, 3));

            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetAxis("Horizontal");

            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetAxis("Vertical");

            transform.Translate(new Vector2(moveHorizontal, moveVertical) * speed * Time.deltaTime);
        }
    }
}