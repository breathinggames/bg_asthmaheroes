﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    [System.Serializable]
    public class Guns
    {
        // guns objects in Player hierarchy
        public GameObject rightGun, leftGun, centralGun;
        [HideInInspector] public ParticleSystem leftGunVFX, rightGunVFX, centralGunVFX;
    }
}

namespace BreathingGames.ArcadeVertShooter
{
    public class PlayerShooting : MonoBehaviour
    {

        [Tooltip("shooting frequency. the higher the more frequent")]
        public float fireRate;

        [Tooltip("projectile prefab")]
        public GameObject projectileObject;

        //time for a new shot
        [HideInInspector] public float nextFire;

        private AudioSource audioData;

        [Tooltip("current weapon power")]
        [Range(1, 4)]  
        public int weaponPower = 1;

        public Guns guns;
        bool shootingIsActive = true;
        [HideInInspector] public int maxweaponPower = 4;
        public static PlayerShooting instance;

        void Awake()
        {
            if (instance == null)
                instance = this;
        }

        void Start()
        {
            //receiving shooting visual effects components
            guns.leftGunVFX = guns.leftGun.GetComponent<ParticleSystem>();
            guns.rightGunVFX = guns.rightGun.GetComponent<ParticleSystem>();
            guns.centralGunVFX = guns.centralGun.GetComponent<ParticleSystem>();

            audioData = GetComponent<AudioSource>();
        }

        void Update()
        {
            if (shootingIsActive)
            {
                if (Time.time > nextFire && Input.GetButton("Shooter Fire"))
                {
                    MakeAShot();
                    nextFire = Time.time + 1 / fireRate;
                }
            }
        }

        /// <summary>
        /// method for a shot
        /// according to weapon power 'pooling' the defined anount of projectiles, on the defined position, in the defined rotation
        /// </summary>
        void MakeAShot()
        {
            switch (weaponPower)
            {
                case 1:
                    CreateLazerShot(projectileObject, guns.centralGun.transform.position, Vector3.zero);
                    audioData.Play(0);
                    guns.centralGunVFX.Play();
                    break;
                case 2:
                    CreateLazerShot(projectileObject, guns.rightGun.transform.position, Vector3.zero);
                    audioData.Play(0);
                    guns.leftGunVFX.Play();
                    CreateLazerShot(projectileObject, guns.leftGun.transform.position, Vector3.zero);
                    guns.rightGunVFX.Play();
                    break;
                case 3:
                    CreateLazerShot(projectileObject, guns.centralGun.transform.position, Vector3.zero);
                    CreateLazerShot(projectileObject, guns.rightGun.transform.position, new Vector3(0, 0, -5));
                    audioData.Play(0);
                    guns.leftGunVFX.Play();
                    CreateLazerShot(projectileObject, guns.leftGun.transform.position, new Vector3(0, 0, 5));
                    guns.rightGunVFX.Play();
                    break;
                case 4:
                    CreateLazerShot(projectileObject, guns.centralGun.transform.position, Vector3.zero);
                    CreateLazerShot(projectileObject, guns.rightGun.transform.position, new Vector3(0, 0, -5));
                    audioData.Play(0);
                    guns.leftGunVFX.Play();
                    CreateLazerShot(projectileObject, guns.leftGun.transform.position, new Vector3(0, 0, 5));
                    guns.rightGunVFX.Play();
                    CreateLazerShot(projectileObject, guns.leftGun.transform.position, new Vector3(0, 0, 15));
                    CreateLazerShot(projectileObject, guns.rightGun.transform.position, new Vector3(0, 0, -15));
                    break;
            }
        }

        /// <summary>
        /// translating pooled lazer shot to the defined position in the defined rotation
        /// </summary>
        /// <param name="lazer"></param>
        /// <param name="pos"></param>
        /// <param name="rot"></param>
        void CreateLazerShot(GameObject lazer, Vector3 pos, Vector3 rot)
        {
            Instantiate(lazer, pos, Quaternion.Euler(rot));
        }
    }
}