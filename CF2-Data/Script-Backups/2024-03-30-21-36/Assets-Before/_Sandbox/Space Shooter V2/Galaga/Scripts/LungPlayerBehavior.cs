﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AH_Galaga
{
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class LungPlayerBehavior : MonoBehaviour
    {
        private bool isDragged;
        private Vector3 screenPoint;
        private Vector3 offset;

        // projectile
        public GameObject projectile;
        public Transform[] projectileSpawns;
        public float fireRate = 0.5f;
        public int projectileLevel = 1;
        public int projectileDamage = 1;
        private float nextProjectile;

        // Health
        int health = 1;

        // effects
        public GameObject explosion;

        // reset
        private Vector3 initPos;
        private bool isDead;

        void Start()
        {
            initPos = transform.position;
        }

        void Update()
        {
            if (isDragged && Time.time > nextProjectile && !isDead)
            {
                nextProjectile = Time.time + fireRate;
                for (int i = 0; i < projectileLevel; i++)
                {
                    GameObject newProjectile = Instantiate(projectile, projectileSpawns[i].position, projectileSpawns[i].rotation);
                    newProjectile.GetComponent<LungProjectile>().SetDamage(projectileDamage);
                }
            }
        }

        private void OnMouseDown()
        {
            screenPoint = Camera.main.WorldToScreenPoint(transform.position);
            offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));  // might have to invert y and z
        }

        private void OnMouseDrag()
        {
            if (!isDead)
            {
                Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;

                isDragged = true;
                transform.position = currentPosition; 
            }
        }

        private void OnMouseUp()
        {
            isDragged = false;
        }

        public void TakeDamage(int amount)
        {
            health += amount;

            if (health <= 0)
            {
                // sound and sfx
                if (explosion != null)
                {
                    Instantiate(explosion, transform.position, Quaternion.identity);  
                }

                StartCoroutine(ResetPos());
            }
        }

        private IEnumerator ResetPos()
        {
            AHGManager.instance.DecreaseLife();

            GetComponent<SpriteRenderer>().enabled = false;   
            GetComponent<SphereCollider>().enabled = false;
            isDead = true;       
            
            transform.position = initPos;
            yield return new WaitForSeconds(2f);

            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SphereCollider>().enabled = true;
            isDead = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                TakeDamage(-10);
            }
        }
    }
}