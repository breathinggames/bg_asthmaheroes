﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShooterGameOver : MonoBehaviour
{
    public TextMeshProUGUI highScoreText;
    public TextMeshProUGUI highestLevelText;

    void Start()
    {
        highScoreText.text = "Highscore " + ScoreHolder.score;
        highestLevelText.text = "Highest level  " + ScoreHolder.level;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // SceneManager.LoadScene("");
        }
    }
}
