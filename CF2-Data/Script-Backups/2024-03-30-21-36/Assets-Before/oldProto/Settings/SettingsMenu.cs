﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour 
{
    public Dropdown resolutionDropdown;
    public Toggle isFullscreenToggle;
    public Dropdown qualityDropdown;
    public bool allRefreshRates;

    private int currentResolutionIndex;
    private bool currentFullscreen;
    private List<Resolution> resolutions;

    private void Start()
    {
        // we initialize the list of resolutions and clear the resolution drop down
        resolutions = new List<Resolution>();
        resolutionDropdown.ClearOptions();

        // we get the list of resolutions available to the local system and we add it to the list
        List<string> options = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Resolution resolution = Screen.resolutions[i];
            if (allRefreshRates)
            {
                options.Add(resolution.ToString());
            }
            else if (resolutions.Count == 0)
            {
                options.Add(string.Format("{0} x {1}", resolution.width, resolution.height));
            }
            else
            {
                Resolution previousResolution = resolutions.Last();
                if (resolution.width != previousResolution.width && resolution.height != previousResolution.height)
                {
                    options.Add(string.Format("{0} x {1}", resolution.width, resolution.height));                    
                }
                else if (resolution.refreshRate > previousResolution.refreshRate)
                {
                    resolutions.Remove(previousResolution);
                }
                else
                {
                    continue;
                }
            }
            resolutions.Add(resolution);
        }

        // we also set the default res to the default res set in our system
        currentResolutionIndex = -1;
        for (int i = 0; i < resolutions.Count; i++)
        {
            Resolution resolution = resolutions[i];
            if (Screen.width == resolution.width && Screen.height == resolution.height)
            {
                currentResolutionIndex = i;
                if (!allRefreshRates || Screen.currentResolution.refreshRate == resolution.refreshRate)
                {
                    break;
                }
            }
        }

        // fallback to highest resolution
        if (currentResolutionIndex < 0)
        {
            currentResolutionIndex = resolutions.Count - 1;
        }

        // add the list to the ui and display the current res on startup
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        
        isFullscreenToggle.isOn = Screen.fullScreen;

        qualityDropdown.value = QualitySettings.GetQualityLevel();
    }

    // dynamically change quality settings
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // press toggle to change the screen mode
    public void SetFullscreen(bool isFullscreen)
    {
        currentFullscreen = isFullscreen;
        ChangeResolution();
    }

    public void SetResolution(int resolutionIndex)
    {
        currentResolutionIndex = resolutionIndex;
        ChangeResolution();
    }

    private void ChangeResolution()
    {
        Resolution resolution = resolutions[currentResolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, currentFullscreen,
            allRefreshRates ? resolution.refreshRate : 0);        
    }
}
