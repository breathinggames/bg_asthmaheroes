﻿using BreathingGames.CraftingSystem;
using BreathingGames.InventorySystem.Serialization;
using BreathingGames.PlayerStats;
using UnityEngine;
using UnityEngine.UI;

namespace BreathingGames.InventorySystem
{
    public class InventorySystemManager : MonoBehaviour
    {
        public int healthAmount;

        //use to ref the stats from the character namespace

        [Header("Character Stats Settings")]
        public CharacterStatsManager health;
        public CharacterStatsManager lungCapacity;
        public CharacterStatsManager stamina;
        public CharacterStatsManager allergen;   
        [Space]

        [Header("Reference Settings")]
        public Inventory inventory;
        public EquipmentPanel equipmentPanel;
        [SerializeField] private CraftingWindow craftingWindow;
        [SerializeField] private StatPanel statPanel;
        [SerializeField] private ItemTooltip itemTooltip;
        [SerializeField] private Image draggableItem;
        [SerializeField] private DropItemArea dropItemArea;
        [SerializeField] private QuestionDialog questionDialog;
        [SerializeField] private ItemSaveManager itemSaveManager;

        private BaseItemSlot dragItemSlot;

        private void OnValidate()
        {
            if (itemTooltip == null)
            {
                itemTooltip = FindObjectOfType<ItemTooltip>();
            }
        }

        void Awake()
        {
            statPanel.SetStats(health, stamina, lungCapacity, allergen);
            statPanel.UpdateStatValues();

            // Setup Events:
            // Right CLick
            inventory.OnRightClickEvent += InventoryRightClick;
            equipmentPanel.OnRightClickEvent += EquipmentPanelRightClick;

            // Pointer Enter
            inventory.OnPointerEnterEvent += ShowTooltip;
            equipmentPanel.OnPointerEnterEvent += ShowTooltip;
            craftingWindow.OnPointerEnterEvent += ShowTooltip;

            // Pointer Exit
            inventory.OnPointerExitEvent += HideTooltip;
            equipmentPanel.OnPointerExitEvent += HideTooltip;
            craftingWindow.OnPointerExitEvent += HideTooltip;

            // Begin Drap
            inventory.OnBeginDragEvent += BeginDrag;
            equipmentPanel.OnBeginDragEvent += BeginDrag;

            // End Drag
            inventory.OnEndDragEvent += EndDrag;
            equipmentPanel.OnEndDragEvent += EndDrag;

            // Drag
            inventory.OnDragEvent += Drag;
            equipmentPanel.OnDragEvent += Drag;

            // Drop item from one slot to another
            inventory.OnDropEvent += Drop;
            equipmentPanel.OnDropEvent += Drop;

            // Drop and delete from inventory
            dropItemArea.OnDropEvent += DropItemOutsideUI;
        }

        private void Start()
        {
            itemSaveManager.LoadEquipment(this);
            itemSaveManager.LoadInventory(this);
        }

        /// <summary>
        /// when destroyed, save the database
        /// </summary>
        private void OnDestroy()
        {
            itemSaveManager.SaveInventory(this);
            itemSaveManager.SaveEquipment(this);
        }

        /// <summary>
        /// When right clicking on an item from the inventory window, depending on its type, perform the appropriate action
        /// </summary>
        /// <param name="itemSlot"></param>
        private void InventoryRightClick(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item is EquippableItem)
            {
                Equip((EquippableItem)itemSlot.Item);
            }
            else if (itemSlot.Item is UsableItem)
            {
                UsableItem usableItem = (UsableItem)itemSlot.Item;
                usableItem.Use(this);

                if (usableItem.isConsumable)
                {
                    inventory.RemoveItem(usableItem);
                    usableItem.Destroy();
                }
            }
        }

        /// <summary>
        /// When right clicking on an item from the equipment window, unequip it
        /// </summary>
        /// <param name="itemSlot"></param>
        private void EquipmentPanelRightClick(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item is EquippableItem)
            {
                Unequip((EquippableItem)itemSlot.Item);
            }
        }

        /// <summary>
        /// Show the tooltip
        /// </summary>
        /// <param name="itemSlot"></param>
        private void ShowTooltip(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item != null)
            {
                itemTooltip.ShowTooltip(itemSlot.Item);
            }
        }

        /// <summary>
        /// Hide the tooltip
        /// </summary>
        /// <param name="itemSlot"></param>
        private void HideTooltip(BaseItemSlot itemSlot)
        {
            if (itemTooltip.gameObject.activeSelf)
            {
                itemTooltip.HideTooltip();
            }           
        }

        /// <summary>
        /// Actions to perform at the beginning of the drag
        /// </summary>
        /// <param name="itemSlot"></param>
        private void BeginDrag(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item != null)
            {
                dragItemSlot = itemSlot;
                draggableItem.sprite = itemSlot.Item.icon;
                draggableItem.transform.position = ControlFreak2.CF2Input.mousePosition;
                draggableItem.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Actions to perform at the end of the drag
        /// </summary>
        /// <param name="itemSlot"></param>
        private void EndDrag(BaseItemSlot itemSlot)
        {
            dragItemSlot = null;
            draggableItem.gameObject.SetActive(false);
        }

        /// <summary>
        /// Action to perfom while dragging
        /// </summary>
        /// <param name="itemSlot"></param>
        private void Drag(BaseItemSlot itemSlot)
        {
            draggableItem.transform.position = ControlFreak2.CF2Input.mousePosition;          
        }

        /// <summary>
        /// Action to perfom while dropping the item on a target
        /// </summary>
        /// <param name="dropItemSlot"></param>
        private void Drop(BaseItemSlot dropItemSlot)
        {
            if (dragItemSlot == null)
            {
                return;
            }

            if (dropItemSlot.CanAddStack(dragItemSlot.Item))
            {
                AddStacks(dropItemSlot);
            }
            else if (dropItemSlot.CanReceiveItem(dragItemSlot.Item) && dragItemSlot.CanReceiveItem(dropItemSlot.Item))
            {
                SwapItems(dropItemSlot);
            }
        }

        /// <summary>
        /// Drop item by dragging it out of the UI to delete it
        /// </summary>
        private void DropItemOutsideUI()
        {
            if (dragItemSlot == null)
            {
                return;
            }

            questionDialog.Show();

            // capture the value of the var in a temp var to prevent a null ref exception
            BaseItemSlot baseItemSlot = dragItemSlot;

            // the OnYesEvent is a void method with 0 arguments while the DestroyItemInSlot has 1 arguments. They are incompatible
            // We create a lambda expression that will create a new method that has 0 arguments and it will call DestroyItemInSlot and return it to OnYesEvent
            questionDialog.OnYesEvent += () => DestroyItemInSlot(baseItemSlot);
        }

        /// <summary>
        /// Destroy an item from the inventory
        /// </summary>
        /// <param name="baseItemSlot"></param>
        private void DestroyItemInSlot(BaseItemSlot baseItemSlot)
        {
            baseItemSlot.Item.Destroy();
            baseItemSlot.Item = null;
        }

        /// <summary>
        /// Exchange two different stacks from their respective slots when dragging if the slot can receive Items
        /// </summary>
        /// <param name="dropItemSlot"></param>
        private void SwapItems(BaseItemSlot dropItemSlot)
        {
            EquippableItem dragItem = dragItemSlot.Item as EquippableItem;
            EquippableItem dropItem = dropItemSlot.Item as EquippableItem;

            if (dropItemSlot is EquipmentSlot)
            {
                if (dragItem != null)
                {
                    dragItem.Equip(this);
                }

                if (dropItem != null)
                {
                    dropItem.Unequip(this);
                }
            }

            if (dragItemSlot is EquipmentSlot)
            {
                if (dragItem != null)
                {
                    dragItem.Unequip(this);
                }

                if (dropItem != null)
                {
                    dropItem.Equip(this);
                }
            }

            statPanel.UpdateStatValues();

            Item draggedItem = dragItemSlot.Item;
            int draggedItemAmount = dragItemSlot.Amount;

            dragItemSlot.Item = dropItemSlot.Item;
            dragItemSlot.Amount = dropItemSlot.Amount;

            dropItemSlot.Item = draggedItem;
            dropItemSlot.Amount = draggedItemAmount;
        }

        /// <summary>
        /// We add the stack to another. If we reach the maximum amount of item in that stack, the stack is divided.
        /// </summary>
        /// <param name="dropItemSlot"></param>
        private void AddStacks(BaseItemSlot dropItemSlot)
        {
            int numAddableStacks = dropItemSlot.Item.MaximumStack - dropItemSlot.Amount;
            int stacksToAdd = Mathf.Min(numAddableStacks, dragItemSlot.Amount);

            // add the amount of item in the dragged stack to the new slot up to its maximum amount and substract the same amount from the original slot
            dropItemSlot.Amount += stacksToAdd;
            dragItemSlot.Amount -= stacksToAdd;
        }

        /// <summary>
        /// Equip a new item from the inventory
        /// </summary>
        /// <param name="item"></param>
        public void Equip(EquippableItem item)
        {
            //if we remove item from the inventory to be equipped
            if (inventory.RemoveItem(item))
            {
                //if an item is added to the equipment panel to be equipped
                EquippableItem previousItem;
                if (equipmentPanel.AddItem(item, out previousItem))
                {
                    // if something is already in the selected equipment slot return the previous item that was equipped to the inventory
                    if (previousItem != null)
                    {
                        inventory.AddItem(previousItem);
                        previousItem.Unequip(this);
                        statPanel.UpdateStatValues();
                    }
                    item.Equip(this);
                    statPanel.UpdateStatValues();
                }
                else
                {
                    // if we couldn't equip the selected item from the inventory
                    inventory.AddItem(item);
                }
            }
        }

        /// <summary>
        /// unequip an item from the equipment panel and set it back into the inventory
        /// </summary>
        /// <param name="item"></param>
        public void Unequip(EquippableItem item)
        {
            //check if inventory isn't full first
            if (inventory.CanAddItem(item) && equipmentPanel.RemoveItem(item))
            {
                item.Unequip(this);
                statPanel.UpdateStatValues();

                //return item to the inventory
                inventory.AddItem(item);
            }
        }

        /// <summary>
        /// update the stat value of the stats panel when equipping, unequipping or using items from the inventory
        /// </summary>
        public void UpdateStatValues()
        {
            statPanel.UpdateStatValues();
        }

        // store a ref to the item containe currently open. Useful if we have more than one in the game
        private ItemContainer openItemContainerRef;

        /// <summary>
        /// transfer the item from the inventory to the item container
        /// </summary>
        /// <param name="itemSlot"></param>
        private void TransferToItemContainer(BaseItemSlot itemSlot)
        {
            Item item = itemSlot.Item;
            if (item != null && openItemContainerRef.CanAddItem(item))
            {
                // remove from the inventory and add to the item container
                inventory.RemoveItem(item);
                openItemContainerRef.AddItem(item);
            }
        }

        /// <summary>
        /// transfer the item from the item container to the inventory
        /// </summary>
        /// <param name="itemSlot"></param>
        private void TransferToInventory(BaseItemSlot itemSlot)
        {
            Item item = itemSlot.Item;
            if (item != null && inventory.CanAddItem(item))
            {
                // remove from the item container and add to the inventory           
                openItemContainerRef.RemoveItem(item);
                inventory.AddItem(item);
            }
        }

        /// <summary>
        /// Open the item container and change the how the action events works depending on the state of the window (open/close)
        /// </summary>
        /// <param name="itemContainer"></param>
        public void OpenItemContainer(ItemContainer itemContainer)
        {
            openItemContainerRef = itemContainer;

            // modify events logic
            inventory.OnRightClickEvent -= InventoryRightClick;
            inventory.OnRightClickEvent += TransferToItemContainer;

            itemContainer.OnRightClickEvent += TransferToInventory;

            itemContainer.OnPointerEnterEvent += ShowTooltip;
            itemContainer.OnPointerExitEvent += HideTooltip;
            itemContainer.OnBeginDragEvent += BeginDrag;
            itemContainer.OnEndDragEvent += EndDrag;
            itemContainer.OnDragEvent += Drag;
            itemContainer.OnDropEvent += Drop;
        }

        /// <summary>
        /// close item container and return the action events to their proper working
        /// </summary>
        /// <param name="itemContainer"></param>
        public void CloseItemContainer(ItemContainer itemContainer)
        {
            openItemContainerRef = null;

            // return events logic to normal
            inventory.OnRightClickEvent += InventoryRightClick;
            inventory.OnRightClickEvent -= TransferToItemContainer;

            itemContainer.OnRightClickEvent -= TransferToInventory;

            itemContainer.OnPointerEnterEvent -= ShowTooltip;
            itemContainer.OnPointerExitEvent -= HideTooltip;
            itemContainer.OnBeginDragEvent -= BeginDrag;
            itemContainer.OnEndDragEvent -= EndDrag;
            itemContainer.OnDragEvent -= Drag;
            itemContainer.OnDropEvent -= Drop;
        }
    }
}