﻿using UnityEngine;

namespace BreathingGames.ArcadeVertShooter
{
    [System.Serializable]
    public class Borders
    {
        // defines the borders of Player movement.
        [Tooltip("offset from viewport borders for player's movement")]
        public float minXOffset = 1.5f, maxXOffset = 1.5f, minYOffset = 1.5f, maxYOffset = 1.5f;
        [HideInInspector] public float minX, maxX, minY, maxY;
    }
}

namespace BreathingGames.ArcadeVertShooter
{
    public class PlayerMoving : MonoBehaviour
    {
        [Tooltip("offset from viewport borders for player's movement")]
        public Borders borders;
        Camera mainCamera;
        bool controlIsActive = true;

        //unique instance of the script for easy access to the script
        public static PlayerMoving instance;
        public float speed;

        void Awake()
        {
            if (instance == null)
                instance = this;
        }

        void Start()
        {
            mainCamera = Camera.main;

            //setting Player moving borders depending on Viewport's size
            ResizeBorders();
        }

        void Update()
        {
            if (controlIsActive)
            {

#if UNITY_STANDALONE || UNITY_EDITOR //if the current platform is not mobile, setting mouse handling or key handling

                /*
                if (Input.GetMouseButton(0)) //if mouse button was pressed       
                {
                    Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition); //calculating mouse position in the worldspace
                    mousePosition.z = transform.position.z;
                    transform.position = Vector3.MoveTowards(transform.position, mousePosition, 30 * Time.deltaTime);
                }
                */

                // arrows or wasd for ship control
                if (ControlFreak2.CF2Input.GetKey(KeyCode.LeftArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.A))
                {
                    transform.position += Vector3.left * speed * Time.deltaTime;
                }
                if (ControlFreak2.CF2Input.GetKey(KeyCode.RightArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.D))
                {
                    transform.position += Vector3.right * speed * Time.deltaTime;
                }
                if (ControlFreak2.CF2Input.GetKey(KeyCode.UpArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.W))
                {
                    transform.position += Vector3.up * speed * Time.deltaTime;
                }
                if (ControlFreak2.CF2Input.GetKey(KeyCode.DownArrow) || ControlFreak2.CF2Input.GetKey(KeyCode.S))
                {
                    transform.position += Vector3.down * speed * Time.deltaTime;
                }
#endif

#if UNITY_IOS || UNITY_ANDROID //if current platform is mobile

                // if there is a touch
                if (ControlFreak2.CF2Input.touchCount == 1) 
                {
                    // calculating touch position in the world space
                    ControlFreak2.InputRig.Touch touch = ControlFreak2.CF2Input.touches[0];
                    Vector3 touchPosition = mainCamera.ScreenToWorldPoint(touch.position);  
                    touchPosition.z = transform.position.z;
                    transform.position = Vector3.MoveTowards(transform.position, touchPosition, 30 * Time.deltaTime);
                }
#endif

                //if 'Player' crossed the movement borders, returning him back 
                transform.position = new Vector3(Mathf.Clamp(transform.position.x, borders.minX, borders.maxX),
                                                 Mathf.Clamp(transform.position.y, borders.minY, borders.maxY), 0);
            }
        }

        /// <summary>
        /// setting Player movement borders according to Viewport size and defined offset
        /// </summary>
        void ResizeBorders()
        {
            borders.minX = mainCamera.ViewportToWorldPoint(Vector2.zero).x + borders.minXOffset;
            borders.minY = mainCamera.ViewportToWorldPoint(Vector2.zero).y + borders.minYOffset;
            borders.maxX = mainCamera.ViewportToWorldPoint(Vector2.right).x - borders.maxXOffset;
            borders.maxY = mainCamera.ViewportToWorldPoint(Vector2.up).y - borders.maxYOffset;
        }
    }
}