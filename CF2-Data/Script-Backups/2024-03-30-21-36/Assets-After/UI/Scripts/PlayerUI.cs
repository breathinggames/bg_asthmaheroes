﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using BreathingGames.Character;

namespace BreathingGames.UISystem
{
    public class PlayerUI : MonoBehaviour
    {
        // add one for each bar used and add it also to awake
        public Stat health;
        public Stat lungCapacity;
        public Stat stamina;
        public Stat allergen;

        // UI or equip objects
        public Image breatherBlue;
        public Image breatherYellow;
        public Image maskImage;
        public GameObject pills;
        public GameObject mask;
        public GameObject shield;
        public bool isMaskOn;

        // regen field
        [SerializeField] private float healthRegenRate;
        [SerializeField] private float staminaRegenRate;
        [SerializeField] private float allergenRegenRate;

        // bool regen field
        public bool isAllergenRegenActive = false;
        private bool isStaminaRegenActive = false;
        private bool isHealthRegenActive = false;
        private bool isBreatherRegenActiveBlue = false;
        private bool isBreatherRegenActiveYellow = false;

        // ref variables
        private PlayerPlatformerController staminaController;
        private AudioSource audioData;

        void Awake()
        {
            // init UI sliders
            InitUI();

            shield.SetActive(false);
            audioData = GetComponent<AudioSource>();
        }

        void Update()
        {
            staminaController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPlatformerController>();
            
            if (health.CurrentVal == 0)
            {
                Fungus.Flowchart.BroadcastFungusMessage("Restart");

                health.CurrentVal = health.MaxVal;
                lungCapacity.CurrentVal = lungCapacity.MaxVal;
                stamina.CurrentVal = stamina.MaxVal;
                allergen.CurrentVal = allergen.MaxVal;               
            }

            #region SliderUIDecreaseFactor

            if (staminaController.isRunning)
            {
                stamina.CurrentVal -= 0.1f;
            }

            if (stamina.CurrentVal <= 20)
            {
                lungCapacity.CurrentVal -= 0.01f;
            }

            if (allergen.CurrentVal <= 20)
            {
                lungCapacity.CurrentVal -= 0.01f;
            }

            if (lungCapacity.CurrentVal <= 20)
            {
                health.CurrentVal -= 0.01f;
            }

            #endregion

            // Use the blue breather if its timer slider is full and reset it to 0 after
            BreatherBlueController();
            StartCoroutine(AutoRegenBreatherBlue());

            // Taking the yellow breather restore the optimal decrease amount for all stats
            BreatherYellowController();
            StartCoroutine(AutoRegenBreatherYellow());

            // equip or unequip mask
            isMaskOn = ChangeGreenMaskOpacityOnButtonPress();

            // shield mechanic with allergen
            ShieldActivation();
            ShieldDeactivator();

            #region RegenMechanics

            if (health.CurrentVal <= health.MaxVal && !isHealthRegenActive && lungCapacity.CurrentVal > 20f)
            {
                StartCoroutine(AutoRegenHealth());
            }

            if (stamina.CurrentVal <= stamina.MaxVal && !isStaminaRegenActive && !staminaController.isRunning)
            {
                StartCoroutine(AutoRegenStamina());
            }

            if (allergen.CurrentVal <= allergen.MaxVal && !shield.activeInHierarchy && !isAllergenRegenActive)
            {
                StartCoroutine(AutoRegenAllergen());
            }

            #endregion

            // can be deleted later
            #region testInputForUI

            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.P))
            {
                health.CurrentVal -= 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.O))
            {
                health.CurrentVal += 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.L))
            {
                stamina.CurrentVal -= 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.K))
            {
                stamina.CurrentVal += 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.I))
            {
                allergen.CurrentVal -= 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.U))
            {
                allergen.CurrentVal += 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.J))
            {
                lungCapacity.CurrentVal -= 10;
            }
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.H))
            {
                lungCapacity.CurrentVal += 10;
            }

            #endregion
        }

        private void InitUI()
        {
            health.Initialize();
            stamina.Initialize();
            allergen.Initialize();
            lungCapacity.Initialize();
        }

        private void ShieldDeactivator()
        {
            if (allergen.CurrentVal <= 100 && shield.activeInHierarchy)
            {
                allergen.MaxVal = 100;
                allergen.CurrentVal = allergen.CurrentVal;
                shield.SetActive(false);
            }
        }

        private void ShieldActivation()
        {
            if (pills.activeInHierarchy && ControlFreak2.CF2Input.GetKeyDown(KeyCode.N))
            {
                if (allergen.CurrentVal == allergen.MaxVal && !shield.activeInHierarchy)
                {
                    shield.SetActive(true);
                    audioData.Play(0);
                    allergen.MaxVal = 200;
                    allergen.CurrentVal = 200;
                    pills.SetActive(false);
                }
                else
                {
                    audioData.Play(0);
                    allergen.CurrentVal += 50;
                    pills.SetActive(false);
                }
            }
        }

        private bool ChangeGreenMaskOpacityOnButtonPress()
        {
            if (mask.activeInHierarchy && ControlFreak2.CF2Input.GetKeyDown(KeyCode.M))
            {
                var tempColor = maskImage.color;

                if (tempColor.a == 1f)
                {
                    tempColor.a = .5f;
                    isMaskOn = false;
                }
                else
                {
                    tempColor.a = 1f;
                    isMaskOn = true;
                }
                maskImage.color = tempColor;
            }

            return isMaskOn;
        }

        private void BreatherYellowController()
        {
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.Y) && breatherYellow.fillAmount == 1f)
            {
                // call a method that change the amount of all thats that is decreased after actions
                breatherYellow.fillAmount = 0f;
            }
        }

        private void BreatherBlueController()
        {
            if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.B) && breatherBlue.fillAmount == 1f && lungCapacity.CurrentVal < lungCapacity.MaxVal)
            {
                audioData.Play(0);
                lungCapacity.CurrentVal += 20;
                breatherBlue.fillAmount = 0f;
            }
        }

        private IEnumerator AutoRegenBreatherBlue()
        {
            isBreatherRegenActiveBlue = true;
            while (breatherBlue.fillAmount < 1)
            {
                breatherBlue.fillAmount += 0.0001f;
                yield return new WaitForSeconds(3f);
            }
            isBreatherRegenActiveBlue = false;
        }

        private IEnumerator AutoRegenBreatherYellow()
        {
            isBreatherRegenActiveYellow = true;
            while (breatherYellow.fillAmount < 1)
            {
                breatherYellow.fillAmount += 0.0001f;
                yield return new WaitForSeconds(3000f);
            }
            isBreatherRegenActiveYellow = false;
        }

        private IEnumerator AutoRegenStamina()
        {
            isStaminaRegenActive = true;
            while (stamina.CurrentVal < stamina.MaxVal)
            {
                stamina.CurrentVal++;
                yield return new WaitForSeconds(staminaRegenRate);
            }
            isStaminaRegenActive = false;
        }

        private IEnumerator AutoRegenAllergen()
        {
            isAllergenRegenActive = true;
            while (allergen.CurrentVal < allergen.MaxVal)
            {
                allergen.CurrentVal++;
                yield return new WaitForSeconds(allergenRegenRate);
            }
            isAllergenRegenActive = false;
        }

        private IEnumerator AutoRegenHealth()
        {
            isHealthRegenActive = true;
            while (health.CurrentVal < health.MaxVal)
            {
                health.CurrentVal++;
                yield return new WaitForSeconds(healthRegenRate);
            }
            isHealthRegenActive = false;
        }
    }
}